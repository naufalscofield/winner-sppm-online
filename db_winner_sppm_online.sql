-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2019 at 02:42 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_winner_sppm_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `biro`
--

CREATE TABLE `biro` (
  `id` int(11) NOT NULL,
  `nama_biro` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biro`
--

INSERT INTO `biro` (`id`, `nama_biro`) VALUES
(1, 'IT'),
(2, 'HC'),
(3, 'Umum'),
(4, 'pengadaan');

-- --------------------------------------------------------

--
-- Table structure for table `judul`
--

CREATE TABLE `judul` (
  `id` int(11) NOT NULL,
  `judul` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `judul`
--

INSERT INTO `judul` (`id`, `judul`) VALUES
(1, 'WIKA SOLAR MODULE MODEL MM160C36'),
(2, 'MATERIAL LOKAL DAN MATERIAL BANTU LTSHE');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `material` varchar(50) NOT NULL,
  `kode_material` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `material`, `kode_material`) VALUES
(2, 'tes tes', 'b02'),
(3, 'wakwaw', 'c03'),
(4, 'ww', 'c03'),
(5, 'wakwaw', 'fwef'),
(6, 'aa', 'c03'),
(7, 'wakwawa', 'aooa');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `no_broadcast` varchar(20) NOT NULL,
  `email_perusahaan` varchar(20) NOT NULL,
  `alamat_perusahaan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `no_telp`, `no_broadcast`, `email_perusahaan`, `alamat_perusahaan`) VALUES
(1, '(021) 86863293', '085648435363', 'info@wikaenergi.com', 'Bogor, Kompleks Industri WIKA, Jl. Raya Narogong No.Km. 26, Kembang Kuning, Kec. Klapanunggal, Bogor, Jawa Barat 16810');

-- --------------------------------------------------------

--
-- Table structure for table `spesifikasi_teknik`
--

CREATE TABLE `spesifikasi_teknik` (
  `id` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `spesifikasi_teknik` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spesifikasi_teknik`
--

INSERT INTO `spesifikasi_teknik` (`id`, `id_material`, `spesifikasi_teknik`) VALUES
(2, 6, 'aaaa'),
(3, 5, 'merk abs ukuran 1x1'),
(4, 0, 'spesifikasi_teknik'),
(5, 1, 'aa'),
(6, 2, 'bb'),
(7, 3, 'cc'),
(8, 4, 'dd');

-- --------------------------------------------------------

--
-- Table structure for table `sppm`
--

CREATE TABLE `sppm` (
  `id` int(11) NOT NULL,
  `no_sppm` varchar(50) DEFAULT NULL,
  `id_judul` int(11) DEFAULT NULL,
  `keterangan` text,
  `jenis_sppm` enum('SPPM','Revisi SPPM') NOT NULL,
  `no_spk` varchar(20) DEFAULT NULL,
  `no_rkm` varchar(20) DEFAULT NULL,
  `forecast` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `total_pemesanan` int(11) DEFAULT NULL,
  `tanggal_dibuat` datetime DEFAULT NULL,
  `id_peminta` int(11) NOT NULL,
  `id_manager` int(11) NOT NULL,
  `id_koordinator` int(11) NOT NULL,
  `id_staf_pengadaan` int(11) DEFAULT NULL,
  `target_delivery` date NOT NULL,
  `kategori` enum('SPPM Produk','SPPM Material') NOT NULL,
  `status` enum('waiting','acc_peminta','acc_koordinator','acc_manager','acc_pengadaan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sppm`
--

INSERT INTO `sppm` (`id`, `no_sppm`, `id_judul`, `keterangan`, `jenis_sppm`, `no_spk`, `no_rkm`, `forecast`, `stok`, `total_pemesanan`, `tanggal_dibuat`, `id_peminta`, `id_manager`, `id_koordinator`, `id_staf_pengadaan`, `target_delivery`, `kategori`, `status`) VALUES
(6, 'no/hh/101', 1, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-10-31 05:57:00', 12, 10, 0, 12, '2019-11-02', 'SPPM Produk', 'waiting'),
(7, 'no/123', 1, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-04 10:00:47', 12, 10, 0, 12, '2019-11-05', 'SPPM Produk', 'waiting'),
(8, 'no/123', 1, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-04 10:00:50', 12, 10, 0, 0, '2019-11-05', 'SPPM Produk', 'waiting'),
(9, 'no/1111', 1, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-04 10:02:36', 12, 10, 0, 0, '2019-11-12', 'SPPM Produk', 'waiting'),
(10, 'no/1111', 1, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-04 10:03:27', 12, 10, 11, 0, '2019-11-14', 'SPPM Produk', 'waiting'),
(11, 'nooo', 2, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-06 03:13:06', 12, 10, 0, 0, '2019-11-07', 'SPPM Produk', 'waiting'),
(12, 'aaaa', 1, NULL, 'Revisi SPPM', '', '', 0, 0, NULL, '2019-11-06 09:16:23', 12, 10, 11, 16, '2019-11-07', 'SPPM Produk', 'acc_pengadaan'),
(13, 'nonoo', 2, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-06 13:22:44', 12, 10, 0, 0, '2019-11-14', 'SPPM Produk', 'waiting'),
(14, 'huh', 2, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-07 09:39:49', 12, 10, 11, 0, '2019-11-08', 'SPPM Produk', 'waiting'),
(15, 'no///noooooo', NULL, 'ngoding', 'SPPM', NULL, NULL, NULL, NULL, NULL, '2019-11-08 09:45:08', 12, 10, 11, 13, '2019-11-09', 'SPPM Material', 'acc_pengadaan'),
(16, 'qwdqw', 2, NULL, 'SPPM', '', '', 0, 0, NULL, '2019-11-11 10:20:11', 12, 10, 11, NULL, '2019-11-12', 'SPPM Produk', 'waiting'),
(17, 'a', NULL, 'kerja', 'SPPM', NULL, NULL, NULL, NULL, NULL, '2019-11-13 14:37:24', 12, 10, 11, NULL, '2019-11-14', 'SPPM Material', 'waiting'),
(18, '426/46/11/2019', NULL, 'kerja atuh', 'Revisi SPPM', NULL, NULL, NULL, NULL, NULL, '2019-11-13 14:38:43', 12, 10, 11, NULL, '2019-11-14', 'SPPM Material', 'waiting');

-- --------------------------------------------------------

--
-- Table structure for table `sppm_material`
--

CREATE TABLE `sppm_material` (
  `id` int(11) NOT NULL,
  `id_sppm` int(11) DEFAULT NULL,
  `id_material` int(11) DEFAULT NULL,
  `material_mat` varchar(50) DEFAULT NULL,
  `merek` varchar(50) DEFAULT NULL,
  `id_spesifikasi_teknik` int(11) DEFAULT NULL,
  `spesifikasi_teknik_mat` text,
  `qty` int(11) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `qty_forecast` int(11) DEFAULT NULL,
  `rijection_rate` int(11) DEFAULT NULL,
  `total_forecast` int(11) DEFAULT NULL,
  `satuan_forecast` varchar(10) DEFAULT NULL,
  `harga` int(11) NOT NULL,
  `file` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sppm_material`
--

INSERT INTO `sppm_material` (`id`, `id_sppm`, `id_material`, `material_mat`, `merek`, `id_spesifikasi_teknik`, `spesifikasi_teknik_mat`, `qty`, `satuan`, `qty_forecast`, `rijection_rate`, `total_forecast`, `satuan_forecast`, `harga`, `file`) VALUES
(1, 10, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(2, 10, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(3, 10, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(4, 10, 6, NULL, 'chang', 0, NULL, 2, 'pcs', 20, 2000, 40, 'pcss', 3000, 'key.png'),
(5, 10, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(6, 12, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(7, 12, 2, NULL, '-', 6, NULL, 0, '-', 0, 0, 0, '-', 0, ''),
(8, 12, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(9, 12, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(10, 12, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(11, 14, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(12, 14, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(13, 14, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(14, 16, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(15, 16, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(16, 16, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(17, 15, NULL, 'kabel data', 'asus', NULL, '1 meter', 1, 'pcs', NULL, NULL, NULL, NULL, 20000, 'kementrian2.jpg'),
(18, 15, NULL, 'botol', 'aqua', NULL, '400ml', 1, 'pcs', NULL, NULL, NULL, NULL, 10000, NULL),
(19, 15, NULL, 'buku', 'kikip', NULL, 'bigboss', 1, 'pcs', NULL, NULL, NULL, NULL, 100, 'SPPM_Online_Wika_Industri_Energi.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `t_material`
--

CREATE TABLE `t_material` (
  `id` int(11) NOT NULL,
  `id_judul` int(11) NOT NULL,
  `id_material` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_material`
--

INSERT INTO `t_material` (`id`, `id_judul`, `id_material`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(8, 1, 6),
(9, 1, 7),
(10, 2, 2),
(11, 2, 3),
(12, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nip` varchar(11) NOT NULL,
  `nm_peg` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `status` varchar(10) NOT NULL,
  `jns_kelamin_peg` enum('PRIA','WANITA') NOT NULL,
  `alamat` text NOT NULL,
  `kodepos` int(11) NOT NULL,
  `telepon` varchar(25) DEFAULT NULL,
  `handphone` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `kd_jabatan` int(11) NOT NULL,
  `nm_jabatan` text NOT NULL,
  `kd_unit_org` text NOT NULL,
  `nm_unit_org` text NOT NULL,
  `admin` enum('yes','no') NOT NULL,
  `superadmin` enum('yes','no') NOT NULL,
  `id_biro` int(11) DEFAULT NULL,
  `role` enum('manager','pengadaan','pegawai','koordinator','direksi','admin') NOT NULL,
  `fcm_token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nip`, `nm_peg`, `password`, `status`, `jns_kelamin_peg`, `alamat`, `kodepos`, `telepon`, `handphone`, `email`, `kd_jabatan`, `nm_jabatan`, `kd_unit_org`, `nm_unit_org`, `admin`, `superadmin`, `id_biro`, `role`, `fcm_token`) VALUES
(9, '1', 'Admin', 'e6e061838856bf47e1de730719fb2609', 'aktif', 'PRIA', 'wika industri energi', 1, '01', '01', 'admin@wika.com', 1, 'admin', '1', 'admin', 'yes', 'yes', 1, 'admin', NULL),
(10, 'ES921451', 'MULYONO SISWOMIHARJO', '3811847612be8eb7cc9238c3861e9ff3', 'aktif', 'PRIA', 'Jalan Arafah IV Blok G3/1 Vila Ilhami, Islamic Vilage Karawaci Tangerang', 15560, '', '081380017080', 'mulyono.s@wikamail.id', 505, 'MANAJER BIDANG PROYEK MEGA', '100004001009001014003000000000', 'BIDANG QUALITY ASSURANCE/QUALITY CONTROL PROYEK PEKERJAAN ENGINEERING, PROCUREMENT &amp; CONSTRUCTION (EPC) PEMBANGUNAN TERMINAL LPG REFRIGERATED TANJUNG SEKONG DI MERAK MAS BANTEN (KONSORSIUM PT WIJAYA KARYA (PERSERO) TBK - POSCO ENGINEERING COMPANY', 'no', 'no', 2, 'manager', NULL),
(11, 'ES921459', 'SUWITO', '3811847612be8eb7cc9238c3861e9ff3', 'aktif', 'PRIA', 'JALAN CANDISARI 27, DESA MOJOLANGU KEC LOWOKWARU KOTA MALANG', 0, '', '085239807649', 'suwito@wikamail.id', 403, 'MANAJER PROYEK BESAR', '100003002010020000000000000000', 'PROYEK PEMBANGUNAN JALAN TOL GEMPOL-PASURUAN SEKSI 3A RUAS PASURUAN-GRATI', 'no', 'no', 3, 'koordinator', NULL),
(12, 'LS153474', 'IRVANSYAH IBNU FAISAL', '3811847612be8eb7cc9238c3861e9ff3', 'aktif', 'PRIA', 'JL. PULOMAS SELATAN NO. 22', 13210, '', '081260559449', 'i.ibnu@wikamail.id', 709, 'STAF PROYEK', '100003002001033002001000000000', 'SEKSI AKUNTANSI  PROYEK PEMBANGUNAN JALAN TOL RUAS PEKANBARU - PADANG SEKSI BANGKINANG - PANGKALAN DIVISI OPERASI 1', 'no', 'no', 1, 'pegawai', NULL),
(13, 'ET153507', 'HENRY PRASAKTI IRAWAN', '3811847612be8eb7cc9238c3861e9ff3', 'aktif', 'PRIA', '', 0, '', '081383225880', 'henry.irawan@wika.co.id', 608, 'AHLI MUDA', '100004001009001030000000000000', 'SEKSI ENJINIRING PIPING PROYEK PEKERJAAN ENGINEERING, PROCUREMENT &amp; CONSTRUCTION (EPC) PEMBANGUNAN TERMINAL LPG REFRIGERATED TANJUNG SEKONG DI MERAK MAS BANTEN (KONSORSIUM PT WIJAYA KARYA (PERSERO) TBK - POSCO ENGINEERING COMPANY)', 'no', 'no', 4, 'pengadaan', NULL),
(16, 'ET143434', 'HARAPAN ARYTO SALOM FERDINAND', '3811847612be8eb7cc9238c3861e9ff3', 'aktif', 'PRIA', 'JL. SM RAJA NO.42, GG. GEREJA', 0, '', '081268873456', 'harapan.aryto@wikamail.id', 709, 'STAF PROYEK', '100004001009001014007001000000', 'UNIT PELAKSANA PROYEK PEKERJAAN ENGINEERING, PROCUREMENT &amp; CONSTRUCTION (EPC) PEMBANGUNAN TERMINAL LPG REFRIGERATED TANJUNG SEKONG DI MERAK MAS BANTEN (KONSORSIUM PT WIJAYA KARYA (PERSERO) TBK - POSCO ENGINEERING COMPANY)', 'no', 'no', 4, 'pengadaan', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biro`
--
ALTER TABLE `biro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `judul`
--
ALTER TABLE `judul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spesifikasi_teknik`
--
ALTER TABLE `spesifikasi_teknik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sppm`
--
ALTER TABLE `sppm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sppm_material`
--
ALTER TABLE `sppm_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_material`
--
ALTER TABLE `t_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_biro` (`id_biro`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biro`
--
ALTER TABLE `biro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `judul`
--
ALTER TABLE `judul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `spesifikasi_teknik`
--
ALTER TABLE `spesifikasi_teknik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sppm`
--
ALTER TABLE `sppm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `sppm_material`
--
ALTER TABLE `sppm_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `t_material`
--
ALTER TABLE `t_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `statusDone` ON SCHEDULE EVERY 1 HOUR STARTS '2019-10-10 16:02:59' ON COMPLETION NOT PRESERVE ENABLE DO update tickets set status="done" where tanggal_close < now() and status="closed"$$

CREATE DEFINER=`root`@`localhost` EVENT `statusdonemenit` ON SCHEDULE EVERY 1 MINUTE STARTS '2019-10-10 16:07:32' ON COMPLETION NOT PRESERVE ENABLE DO update tickets set status="done" where tanggal_close < now() and status="closed"$$

CREATE DEFINER=`root`@`localhost` EVENT `statusdoneday` ON SCHEDULE EVERY 1 DAY STARTS '2019-10-10 16:17:09' ON COMPLETION NOT PRESERVE ENABLE DO update tickets 
set status="done" 
where tanggal_close < now() 
  and status="closed"$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
