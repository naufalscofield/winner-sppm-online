<?php
    class Main_model extends CI_Model{

        public function __construct()
        {
            parent::__construct();
        }

        public function getJudul($id = 0)
        {
            if ($id !== 0){
                return $this->db->get_where('judul',['id' => $id])->row();
            } else {
                return $this->db->get('judul')->result_array();
            }
        }

        public function getManager()
        {
            return $this->db->get_where('users',['role' => 'manager'])->result_array();
        }
        
        public function getMaterial()
        {
            return $this->db->get('material')->result_array();
        }

        public function getKoordinator()
        {
            return $this->db->get_where('users',['role' => 'koordinator'])->result_array();
        }

        public function getTMaterial($id)
        {
            $this->db->join('material a','a.id = c.id_material');
            $this->db->join('judul b','b.id = c.id_judul');
            return $this->db->get_where('t_material c',['c.id_judul' => $id])->result_array();
        }

        public function getSPPM($id_sppm = 0)
        {
            if(!empty($id_sppm)) {
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->select('*,judul.id as idjudul, sppm.id as idsppm');
                return $this->db->get_where('sppm',['sppm.id' => $id_sppm])->row();
            } else {
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->select('*,judul.id as idjudul, sppm.id as idsppm');
                return $this->db->get('sppm')->result_array();
            }
                
        }
        
        public function getSPPM2($id_sppm = 0)
        {
            if(!empty($id_sppm)) {
                return $this->db->get_where('sppm',['sppm.id' => $id_sppm])->row();
            } else {
                return $this->db->get('sppm')->result_array();
            }
                
        }
        
        public function getSPPM2Report($id_sppm = 0)
        {
            if(!empty($id_sppm)) {
                $this->db->join('users a','a.id = e.id_peminta');
                $this->db->join('users b','b.id = e.id_koordinator');
                $this->db->join('users c','c.id = e.id_manager');
                $this->db->join('users d','d.id = e.id_staf_pengadaan');
                $this->db->select('*,a.nm_peg as nama_peminta, ,b.nm_peg as nama_koordinator, c.nm_peg as nama_manager, d.nm_peg as nama_staf_pengadaan');
                return $this->db->get_where('sppm e',['e.id' => $id_sppm])->row();
            } else {
                return $this->db->get('sppm')->result_array();
            }
                
        }

        public function insertFile($name, $id_sppmmat)
        {
            $data = array(
                'file' => $name
            );
            // print_r($id_sppmmat); die;
            $this->db->where('id', $id_sppmmat);
            $this->db->update('sppm_material',$data);
        }

        public function getSPPMMat2($id)
        {
            return $this->db->get_where('sppm_material',['id_sppm' => $id])->result_array();
        }


       
    }
?>