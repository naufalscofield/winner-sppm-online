<div class="main-panel">

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">
                <input type="hidden" value="<?= $sppm->idsppm;?>" id="id_sppm"></a>
                <input type="hidden" value="<?= $this->session->userdata('role');?>" id="role_session"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Daftar Material Untuk SPPM No <b><?= $sppm->no_sppm;?></b></h4><br>
                            <h5 class="card-title "><b><?= $sppm->judul;?></b></h5><br>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th width="5">
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>Material</b></center>
                                            </th>
                                            <th width="20">
                                                <center><b>Merek</b></center>
                                            </th>
                                            <th>
                                                <center><b>Spesifikasi Teknik</b></center>
                                            </th>
                                            <th>
                                                <center><b>Qty</b></center>
                                            </th>
                                            <th>
                                                <center><b>Satuan</b></center>
                                            </th>
                                            <th>
                                                <center><b>Harga</b></center>
                                            </th>
                                            <th>
                                                <center><b>File</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="detailSPPMMat" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Material</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <input required type="hidden" name="id_sppm_material_u" id="id_sppm_material_u" class="form-control">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="material_edit">
                                <label for="">Nama Material</label>
                                <input required type="hidden" name="id_material_u" id="id_material_u" class="form-control">
                                <input required type="text" name="material_u" id="material_u" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="spesifikasi_teknik_edit">
                                <label for="">Spesifikasi Teknik</label>
                                <input required type="hidden" name="id_spesifikasi_teknik_u" id="id_spesifikasi_teknik_u" class="form-control">
                                <textarea name="spesifikasi_teknik_u" id="spesifikasi_teknik_u" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="merek_edit">
                                <label for="">Merek</label>
                                <input required type="text" name="merek_u" id="merek_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" id="qty">
                                <label for="">QTY</label>
                                <input type="text" id="qty_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" id="satuan_edit">
                                <label for="">Satuan</label>
                                <input required type="text" name="satuan_u" id="satuan_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="qty_forecast_edit">
                                <label for="">QTY Forecast</label>
                                <input required type="text" name="qty_forecast_u" id="qty_forecast_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="rijection_rate_edit">
                                <label for="">Rijection Rate</label>
                                <input type="text" id="rijection_rate_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for=""><b>Total Kebutuhan Forecast Konversi Supplier</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="total_forecast_edit">
                                <label for="">Total</label>
                                <input required type="text" name="total_forecast_u" id="total_forecast_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="satuan_forecast_edit">
                                <label for="">Satuan</label>
                                <input type="text" id="satuan_forecast_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="harga_edit">
                                <label for="">Harga</label>
                                <input required type="text" name="harga_u" id="harga_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                            Perbarui Detail Material
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="attachFile" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Attach File Untuk Material Ini</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form method="post" enctype="multipart/form-data" action="<?= base_url();?>index.php/web/SPPMMat_File">
                    <div class="row">
                        <div class="col-md-12">
                                <input type="hidden" name="uri" id="uri" value="<?php echo $this->uri->segment(3);?>" class="form-control">
                                <input type="hidden" name="id_material_u_2" id="id_material_u_2" class="form-control">
                                <label for="">File</label>
                                <input type="file" name="file_u" id="file_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_aksi_attach" type="submit" class="btn btn-success btn-fill btn-wd">
                            Attach
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="tabelSpesifikasiTeknik" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Spesifikasi Teknik</h5>
                        <h6 id="list_nama" class="modal-title">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="table-responsive ">
                        <table id="example2" class="table">
                            <thead>
                                <tr>
                                    <center><th width=""><b>No</b></th></center>
                                    <center><th width=""><b>Spesifikasi Teknik</b></th></center>
                                    <center><th><b></b></th></center>
                                </tr>
                            </thead>
                            <tbody id="table-row2">
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
    <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
    <script>
    function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterRole");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[6];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
      function getSPPMMat() {
          var id_sppm = $('#id_sppm').val();
            $.get("http://localhost/winnersppmonline/index.php/WEB/SPPMMat/" + id_sppm, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    if( (element.file == null) || (element.file == '') ){
                        var file = '-'
                    } else {
                        var file = 'file'
                    }
                    if ($('#role_session').val() !== 'pengadaan'){
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.material+"</center>",
                        "<center>"+(element.merek || '-')+"</center>",
                        "<center>"+(element.spesifikasi_teknik || '-')+"</center>",
                        "<center>"+(element.qty || '-')+"</center>",
                        "<center>"+(element.satuan || '-')+"</center>",
                        "<center>"+(element.harga || '-')+"</center>",
                        "<center><a href = '<?=base_url();?>assets/file_spesifikasi_teknik_material/"+element.file+"'>"+file+"</a></center>",
                        "<center><button type='button' id='btn_hapus' data-idmaterial="+element.idsppmmaterial+" data-idsppmmaterial="+element.idsppmmaterial+" class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Material Dari SPPM Ini'><i class='fa fa-trash'></i></button><button type='button' id='btn_detail_material' data-iddetail="+element.idsppmmaterial+" class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Detail Material'><i class='fa fa-pencil'></i></button><button type='button' id='btn_attach' data-iddetail="+element.idsppmmaterial+" class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Attach file'><i class='fa fa-paperclip'></i></button></center>"
                    ] ).draw( false );
                    } else {
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.material+"</center>",
                        "<center>"+(element.merek || '-')+"</center>",
                        "<center>"+(element.spesifikasi_teknik || '-')+"</center>",
                        "<center>"+(element.qty || '-')+"</center>",
                        "<center>"+(element.satuan || '-')+"</center>",
                        "<center>"+(element.harga || '-')+"</center>",
                        "<center><a href = '<?=base_url();?>assets/file_spesifikasi_teknik_material/"+element.file+"'>"+file+"</a></center>",
                        "<center></center>"
                    ] ).draw( false );
                    }
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

          function clearTable() 
            {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
            }
          
          function clearTable2() 
            {
            var table = $('#example2').DataTable();
             table
                .clear()
                .draw();
            }

            function clearModalDetail()
            {
                        $("#material_u").val('')
                        $("#merek_u").val('')
                        $("#spesifikasi_teknik_u").val('')
                        $("#qty_u").val('')
                        $("#satuan_u").val('')
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {

            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });

            $(document).on('click', '#spesifikasi_teknik_u' ,function(){
                var id_material = $('#id_material_u').val();
                $.get("http://localhost/winnersppmonline/index.php/WEB/Spesifikasi/" + 0 + '/' + id_material, function(data, status){
                    console.log(data, status)
                    if (status) {
                        $('#table-row2').html("")
                        let no = 0;
                        data.forEach(element => {
                        var t = $('#example2').DataTable();
                        no++;
                        t.row.add( [
                            "<center>"+no+"</center>",
                            "<center>"+(element.spesifikasi_teknik || '-')+"</center>",
                            "<center><button class='btn btn-sm btn-success' id='btn_pilih_spesifikasi' data-spesifikasi_teknik= "+element.spesifikasi_teknik+" data-id_spesifikasi="+element.id+"><i class='fa fa-check'></i></button></center>"
                        ] ).draw( false );
                        });
                    }
                    else {
                        alert('Load Data Failed')
                    }
                });
                $('#tabelSpesifikasiTeknik').modal('show')
            }),
            $(document).on('focus', '#spesifikasi_teknik_u' ,function(){
                var id_material = $('#id_material_u').val();
                $.get("http://localhost/winnersppmonline/index.php/WEB/Spesifikasi/" + 0 + '/' + id_material, function(data, status){
                    console.log(data, status)
                    if (status) {
                        $('#table-row2').html("")
                        let no = 0;
                        data.forEach(element => {
                        var t = $('#example2').DataTable();
                        no++;
                        t.row.add( [
                            "<center>"+no+"</center>",
                            "<center>"+(element.spesifikasi_teknik || '-')+"</center>",
                            "<center><button class='btn btn-sm btn-success' id='btn_pilih_spesifikasi' data-spesifikasi_teknik= "+element.spesifikasi_teknik+" data-id_spesifikasi="+element.id+"><i class='fa fa-check'></i></button></center>"
                        ] ).draw( false );
                        });
                    }
                    else {
                        alert('Load Data Failed')
                    }
                });
                $('#tabelSpesifikasiTeknik').modal('show')
            }),

            $(document).on('click', '#btn_pilih_spesifikasi', function(){
                var id_spesifikasi = $(this).data("id_spesifikasi");
                var spesifikasi = $(this).data("spesifikasi_teknik");
                $('#spesifikasi_teknik_u').val(spesifikasi);
                $('#id_spesifikasi_teknik_u').val(id_spesifikasi);
                $('#tabelSpesifikasiTeknik').modal('hide');
            })

            $('#example').DataTable( {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "columnDefs": [
                    { "width": "20%", "targets": 6 },
                    { "width": "30%", "targets": 3 },
                    { "width": "30%", "targets": 8 },
                    { "width": "20%", "targets": 2 },
                ],
                dom: 'lBfrtip',
            } );
            
            $('#example2').DataTable( {
            } );

            getSPPMMat()

            $("#tabelSpesifikasiTeknik").on("hidden.bs.modal", function () {
                clearTable2()
            } );        
        } );        

            $(document).on('click', '#btn_hapus' ,function(){
                var data = {
                    status : 'acc_peminta'
                    }
                var id = $(this).data("idmaterial");
                jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/SPPMMat/' + id,
                    type: 'delete',
                    data : data,
                    success: function(data) {
                        clearTable()
                        getSPPMMat();
                        toastr.success('Material dihapus dari SPPM!')
                        // location.reload()
                        }
                    });
            });

            $(document).on('click', '#btn_detail_material' ,function(){
                clearModalDetail()
                $('#detailSPPMMat').modal('show')
                var id =  $(this).data("iddetail");
                console.log(id)
                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnersppmonline/index.php/WEB/SPPMMat/' + 0 + '/' + id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)

                        $("#id_sppm_material_u").val((id || '-'))
                        $("#id_material_u").val((resp.idmaterial || '-'))
                        $("#material_u").val((resp.material || '-'))
                        $("#merek_u").val((resp.merek || '-'))
                        $("#spesifikasi_teknik_u").val((resp.spesifikasi_teknik || '-'))
                        $("#qty_u").val((resp.qty) || '-')
                        $("#satuan_u").val((resp.satuan || '-'))
                        $("#qty_forecast_u").val((resp.qty_forecast || '-'))
                        $("#rijection_rate_u").val((resp.rijection_rate || '-'))
                        $("#total_forecast_u").val((resp.total_forecast || '-'))
                        $("#satuan_forecast_u").val((resp.satuan_forecast || '-'))

                    },
                    error: function (jqXHR, exception) {
                    // console.log(jqXHR, exception)
                    }
                });
            });
            
            $(document).on('click', '#btn_attach' ,function(){
                $('#attachFile').modal('show')
                var id_material = $(this).data("iddetail")
                console.log(id_material,'aw')
                $('#id_material_u_2').val(id_material)
            });

            $(document).on('click', '#btn_update' ,function(){
                var id_sppm_material            = $("#id_sppm_material_u").val();
                var id_spesifikasi_teknik       = $("#id_spesifikasi_teknik_u").val();
                var merek                       = $("#merek_u").val();
                var qty                         = $("#qty_u").val();
                var satuan                      = $("#satuan_u").val();
                var qty_forecast                = $("#qty_forecast_u").val();
                var rijection_rate              = $("#rijection_rate_u").val();
                var total_forecast              = $("#total_forecast_u").val();
                var satuan_forecast             = $("#satuan_forecast_u").val();
                var harga                       = $("#harga_u").val();
                console.log(status)

                // if ( (nip == '') || (status == '') || (role == '') || (id_biro == '') )
                // {
                //     toastr.error('Harap lengkapi data!')
                // } else {
                    var data = {
                        id_spesifikasi_teknik   : id_spesifikasi_teknik,
                        merek                   : merek,
                        qty                     : qty,
                        satuan                  : satuan,
                        qty_forecast            : qty_forecast,
                        rijection_rate          : rijection_rate,
                        total_forecast          : total_forecast,
                        satuan_forecast         : satuan_forecast,
                        harga                   : harga,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/SPPMMat/' + id_sppm_material,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#detailSPPMMat').modal('hide')
                        clearModalDetail()
                        clearTable()
                        getSPPMMat();
                        toastr.success('Material sukses diperbarui!')
                        }
                    });
                // }
            });
            $(document).on('click', '#btn_back', function(){
  window.history.back();
})

      </script>