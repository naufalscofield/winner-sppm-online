<div class="main-panel">
      <!-- Navbar -->
      
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">Pegawai</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Pegawai</h4>
                  <button data-toggle="modal" id="addUser" data-target="#addUsers" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Pegawai</button>
                </div>
                <div class="col-md-12">
                      <div class="form-group">
                        <select name="" class="form-control" id="filterRole" onchange="myFunction()">
                          <option value="">-- Filter Role --</option>
                          <option value="">-- Tampilkan Semua --</option>
                          <option value="direksi">Direksi</option>
                          <option value="manager">Manager</option>
                          <option value="koordinator">Koordinator</option>
                          <option value="pengadaan">Pengadaan</option>
                          <option value="pegawai">Pegawai</option>
                        </select>
                      </div>
                    </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>
                            <th><center><b>NIP</b></center></th>
                            <th><center><b>Nama</b></center></th>
                            <th><center><b>JK</b></center></th>
                            <!-- <th><center><b>Telepon</b></center></th> -->
                            <th><center><b>Jabatan</b></center></th>
                            <th><center><b>Biro</b></center></th>
                            <th><center><b>Role</b></center></th>
                            <th><center><b>Status</b></center></th>
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div id="addUsers" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Tambah Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input required type="text" name="nip" id="nip" class="form-control">
                                    <input required type="hidden" name="nip-a" id="nip-a" class="form-control">
                                    <center><button id="cari" class="btn btn-primary ladda-button" data-style="slide-down"><span class="ladda-label">Cari</span></button>
                                    </center>
                                 </div>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="nm_peg_input">
                                <label>Nama Pegawai</label>
                                <input readonly required type="text" name="nm_peg" id="nm_peg" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="status_input">
                                <label>Status</label>
                                <input readonly required type="text" name="status" id="status" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="jns_kelamin_peg_input">
                                <label>Jenis Kelamin</label>
                                <input readonly required type="text" name="jns_kelamin_peg" id="jns_kelamin_peg" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="alamat_input">
                                <label>Alamat</label>
                                <input readonly required type="text" name="alamat" id="alamat" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="kodepos_input">
                                <label>Kodepos</label>
                                <input readonly required type="text" name="kodepos" id="kodepos" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="telepon_input">
                                <label>Telepon</label>
                                <input readonly required type="text" name="telepon" id="telepon" class="form-control">
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="handphone_input">
                                <label>Handphone</label>
                                <input readonly required type="text" name="handphone" id="handphone" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="email_input">
                                <label>Email</label>
                                <input readonly required type="email" name="email" id="email" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="kd_jabatan_input">
                                <label>Kode Jabatan</label>
                                <input readonly required type="text" name="kd_jabatan" id="kd_jabatan" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="nm_jabatan_input">
                                <label>Nama Jabatan</label>
                                <input readonly required type="text" name="nm_jabatan" id="nm_jabatan" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="kd_unit_org_input">
                                <label>Kode Unit Org</label>
                                <input readonly required type="text" name="kd_unit_org" id="kd_unit_org" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="nm_unit_org_input">
                                <label>Nama Unit Org</label>
                                <input readonly required type="text" name="nm_unit_org" id="nm_unit_org" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="id_biro_input">
                                    <label for="">Biro</label>
                                    <!-- <input readonly required type="text" name="status_u" id="status_u" class="form-control"> -->
                                    <select required class="form-control" name="id_biro" id="id_biro">
                                    <option value="">-- Pilih Biro --</option>
                                    <?php foreach ($biro as $biro) {?>
                                    <option  value="<?=$biro['id'];?>"><?=$biro['nama_biro'];?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                       
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="id_biro_input">
                                    <label for="">Role / Jabatan</label>
                                    <select required class="form-control" name="id_biro" id="id_biro">
                                    <option value="">-- Pilih Role/Jabatan --</option>
                                    <option  value="direksi">Direksi</option>
                                    <option  value="manager">Manager</option>
                                    <option  value="koordinator">Koordinator</option>
                                    <option  value="pegawai">Pegawai</option>
                                    <option  value="pengadaan">Pengadaan</option>
                                    </select>
                                </div>
                            </div>
                        </div> -->
            
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_add" type="" disabled class="btn btn-success btn-fill btn-wd">
                                Tambah Pegawai
                            </button>
                            
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       
        <div id="editUsers" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Edit Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="nip_edit">
                                <label for="">NIP</label>
                                <input readonly required type="text" name="nip_u" id="nip_u" class="form-control">
                                 </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="nm_peg_edit">
                                <label for="">Nama Pegawai</label>
                                <input readonly required type="text" name="nm_peg_u" id="nm_peg_u" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="jns_kelamin_peg_edit">
                                <label for="">Jenis Kelamin</label>
                                <input readonly required type="text" name="jns_kelamin_peg_u" id="jns_kelamin_peg_u" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="alamat_edit">
                                <label for="">Alamat</label>
                                <textarea readonly required name="alamat_u" id="alamat_u" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="kodepos_edit">
                                <label for="">Kodepos</label>
                                <input readonly required type="text" name="kodepos_u" id="kodepos_u" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="telepon_edit">
                                <label for="">Telepon</label>
                                <input readonly required type="text" name="telepon_u" id="telepon_u" class="form-control">
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="handphone_edit">
                                <label for="">Handphone</label>
                                <input readonly required type="text" name="handphone_u" id="handphone_u" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="email_edit">
                                <label for="">Email</label>
                                <input readonly required type="text" name="email_u" id="email_u" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="kd_jabatan_edit">
                                <label for="">Kode Jabatan</label>
                                <input readonly required type="text" name="kd_jabatan_u" id="kd_jabatan_u" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="nm_jabatan_edit">
                                <label for="">Nama Jabatan</label>
                                <input readonly required type="text" name="nm_jabatan_u" id="nm_jabatan_u" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="kd_unit_org_edit">
                                <label for="">Kode Unit Org</label>
                                <input readonly required type="text" name="kd_unit_org_u" id="kd_unit_org_u" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="nm_unit_org_edit">
                                <label for="">Nama Unit Org</label>
                                <input readonly required type="text" name="nm_unit_org_u" id="nm_unit_org_u" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="status_edit">
                                    <label for="">Status</label>
                                    <!-- <input readonly required type="text" name="status_u" id="status_u" class="form-control"> -->
                                    <select required class="form-control" name="status_u" id="status_u">
                                    <option value="">-- Pilih Status --</option>
                                    <option value="aktif">Aktif</option>
                                    <option value="nonaktif">Nonaktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="role_eidt">
                                    <label for="">Role / Jabatan</label>
                                    <?php $role = ["direksi","manager","koordinator","pegawai","admin","pengadaan"];?>
                                    <select required class="form-control" name="role_u" id="role_u">
                                    <option value="">-- Pilih Role/Jabatan --</option>
                                    <?php foreach ($role as $role) { ?>
                                    <option value="<?= $role;?>"><?= $role;?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="id_biro_edit">
                                    <label for="">Biro</label>
                                    <!-- <input readonly required type="text" name="status_u" id="status_u" class="form-control"> -->
                                    <select required class="form-control" name="id_biro_u" id="id_biro_u">
                                    <option value="">-- Pilih Biro --</option>
                                    <?php foreach ($biro_ as $biro) {?>
                                    <option class="form-control" value="<?=$biro['id'];?>"><?=$biro['nama_biro'];?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                                Perbarui Pegawai
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
    <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
    <script>
    function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterRole");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[6];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
      function getUsers() {
            $.get("http://localhost/winnersppmonline/index.php/WEB/Pegawai", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.nip+"</center>",
                        "<center>"+element.nm_peg+"</center>",
                        "<center>"+element.jns_kelamin_peg+"</center>",
                        "<center>"+element.nm_jabatan+"</center>",
                        "<center>"+element.nama_biro+"</center>",
                        "<center>"+element.role+"</center>",
                        "<center>"+element.status+"</center>",
                        "<center><a id='btn_delete' data-id="+element.iduser+" class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a><a id='btn_edit' data-idedit="+element.iduser+" class='btn btn-sm btn-info' data-toggle='modal' data-target='#editUser'><i class='fa fa-pencil'></i></a></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

          function clearTable() 
          {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
            }
        function clearModalAdd()
            {
                        $("#nip").val('')
                        $("#nip-a").val('')
                        $("#nm_peg").val('')
                        $("#status").val('')
                        $("#jns_kelamin_peg").val('')
                        $("#alamat").val('')
                        $("#kodepos").val('')
                        $("#telepon").val('')
                        $("#handphone").val('')
                        $("#email").val('')
                        $("#kd_jabatan").val('')
                        $("#nm_jabatan").val('')
                        $("#kd_unit_org").val('')
                        $("#nm_unit_org").val('')
            }
            
            function clearModalEdit()
            {
                        $("#nip_u").val('')
                        $("#nm_peg_u").val('')
                        $("#status_u").val('')
                        $("#jns_kelamin_peg_u").val('')
                        $("#alamat_u").val('')
                        $("#kodepos_u").val('')
                        $("#telepon_u").val('')
                        $("#handphone_u").val('')
                        $("#email_u").val('')
                        $("#kd_jabatan_u").val('')
                        $("#nm_jabatan_u").val('')
                        $("#kd_unit_org_u").val('')
                        $("#nm_unit_org_u").val('')
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $(document).on('click', '#btn_back', function(){
            window.history.back();
            })
            // $('#filterRole').on('change', function(e){
            //     var role = $('#filterRole').val();
            //     var table = $('#example').DataTable();
 
            //     var filteredData = table
            //         .column( 0 )
            //         .data()
            //         .flatten()
            //         .filter( function ( value, index ) {
            //             return value < 2 ? true : false;
            //         } );
            //     console.log(filteredData);
            // });

            $('#example').DataTable( {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'lBfrtip',
            } );

            getUsers()

        } );
            
        $('#cari').on('click', function(e) {
            $('#btn_add').prop('disabled', true);
                $("#nm_peg").val('')
                $("#status").val('')
                $("#jns_kelamin_peg").val('')
                $("#alamat").val('')
                $("#kodepos").val('')
                $("#telepon").val('')
                $("#handphone").val('')
                $("#email").val('')
                $("#kd_jabatan").val('')
                $("#nm_jabatan").val('')
                $("#kd_unit_org").val('')
                $("#nm_unit_org").val('')

                var nip = $('#nip').val()

                e.preventDefault();
                var l = Ladda.create(this);

                l.start();
                $.ajax({
                    type: "GET",
                    url: 'http://hcis.wika.co.id/services/rest/?format=json&method=MasterDataPegawai&wsc_id=WSC-000002&pin=D9u84S&active=1&CMP_id="CMP-000017"&nip='+nip,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp.data)
                    if (resp.data.length > 0){
                        $("#nip-a").val(resp.data[0].nip)
                        $("#nm_peg").val(resp.data[0].nm_peg)
                        $("#status").val(resp.data[0].status)
                        $("#jns_kelamin_peg").val(resp.data[0].jns_kelamin_peg)
                        $("#alamat").val(resp.data[0].alamat)
                        $("#kodepos").val(resp.data[0].kodepos)
                        $("#telepon").val(resp.data[0].telepon)
                        $("#handphone").val(resp.data[0].handphone_1)
                        $("#email").val(resp.data[0].email)
                        $("#kd_jabatan").val(resp.data[0].kd_jabatan)
                        $("#nm_jabatan").val(resp.data[0].nm_jabatan)
                        $("#kd_unit_org").val(resp.data[0].kd_unit_org)
                        $("#nm_unit_org").val(resp.data[0].nm_unit_org)
                        l.stop();
                        $('#btn_add').prop('disabled', false);
                    } else {
                        toastr.error('Data pegawai tidak ditemukan!')
                        l.stop();
                    }

                    },
                    error: function (jqXHR, exception) {
                    // console.log(jqXHR, exception)
                    }
                });
            });

            $(document).on('click', '#btn_add' ,function(){
                var nip                 = $('#nip-a').val();
                var nm_peg              = $('#nm_peg').val();
                var status              = $('#status').val();
                var jns_kelamin_peg     = $('#jns_kelamin_peg').val();
                var alamat              = $('#alamat').val();
                var kodepos             = $('#kodepos').val();
                var telepon             = $('#telepon').val();
                var handphone           = $('#handphone').val();
                var email               = $('#email').val();
                var kd_jabatan          = $('#kd_jabatan').val();
                var nm_jabatan          = $('#nm_jabatan').val();
                var kd_unit_org         = $('#kd_unit_org').val();
                var nm_unit_org         = $('#nm_unit_org').val();
                var id_biro             = $('#id_biro').val();

                    $.post("http://localhost/winnersppmonline/index.php/WEB/CekUsers", {
                        nip                 : nip,
                        },
                        function(data, status){
                        if (data.status == false) {
                            toastr.error(data.message)
                        }
                        else {
                            if (id_biro == '') {
                                toastr.warning('Harap pilih biro!')
                                } else {
                                    $.post("http://localhost/winnersppmonline/index.php/WEB/Pegawai", {
                                        nip                 : nip,
                                        nm_peg              : nm_peg,
                                        status              : $('#status').val(),
                                        jns_kelamin_peg     : jns_kelamin_peg,
                                        alamat              : alamat,
                                        kodepos             : kodepos,
                                        telepon             : telepon,
                                        handphone           : handphone,
                                        email               : email,
                                        kd_jabatan          : kd_jabatan,
                                        nm_jabatan          : nm_jabatan,
                                        kd_unit_org         : kd_unit_org,
                                        nm_unit_org         : nm_unit_org,
                                        id_biro             : id_biro,
                                        role                : 'pegawai',
                                        admin               : 'no', 
                                        superadmin          : 'no', 
                                        },
                                        function(data, status){
                                        if (status) {
                                            toastr.success('Insert Data Success')
                                            clearTable()
                                            $('#addUsers').modal('hide')
                                            clearModalAdd()
                                            getUsers()
                                        }
                                        else {
                                            toastr.error('Insert Data Failed!')
                                        }
                                    });
                            }
                        }
                    });         
            });

            $(document).on('click', '#btn_delete' ,function(){
                // console.log('tes')
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost/winnersppmonline/index.php/WEB/Pegawai/' + id,
                type: 'DELETE',
                success: function(data) {
                    toastr.info('Delete Data Success!')
                    clearTable()
                    getUsers();
                    }
                });
            });

            $(document).on('click', '#btn_edit' ,function(){
                clearModalEdit()
                $('#editUsers').modal('show')
                var id =  $(this).data("idedit");
                console.log(id)
                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnersppmonline/index.php/WEB/Pegawai/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)
                        $("#nip_u").val(resp.nip)
                        $("#nm_peg_u").val(resp.nm_peg)
                        $("#status_u").val(resp.status)
                        $("#jns_kelamin_peg_u").val(resp.jns_kelamin_peg)
                        $("#alamat_u").val(resp.alamat)
                        $("#kodepos_u").val(resp.kodepos)
                        $("#telepon_u").val(resp.telepon)
                        $("#handphone_u").val(resp.handphone)
                        $("#email_u").val(resp.email)
                        $("#kd_jabatan_u").val(resp.kd_jabatan)
                        $("#nm_jabatan_u").val(resp.nm_jabatan)
                        $("#kd_unit_org_u").val(resp.kd_unit_org)
                        $("#nm_unit_org_u").val(resp.nm_unit_org)
                        $("#admin_u").val(resp.admin)
                        $("#superadmin_u").val(resp.superadmin)
                        $("#id_biro_u").val(resp.id_biro)
                        $("#role_u").val(resp.role)
                    
                    },
                    error: function (jqXHR, exception) {
                    // console.log(jqXHR, exception)
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var nip                 = $("#nip_u").val();
                var status              = $("#status_u").val();
                var role                = $("#role_u").val();
                var id_biro             = $("#id_biro_u").val();
                console.log(status)

                if ( (nip == '') || (status == '') || (role == '') || (id_biro == '') )
                {
                    toastr.error('Harap lengkapi data!')
                } else {
                    var data = {
                        status : status,
                        role : role,
                        id_biro : id_biro,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/Pegawai/' + nip,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#editUsers').modal('hide')
                        clearModalEdit()
                        clearTable()
                        getUsers();
                        toastr.success('Update Data Success!')
                        }
                    });
                }
            });

        
      </script>