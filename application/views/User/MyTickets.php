<div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Tickets</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <?= $this->session->userdata('nm_peg');?>
              <input type="hidden" id="id_biro_sess" value="<?= $this->session->userdata('id_biro');?>">
            </form> 
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url();?>index.php/admin/logout">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Ticket Saya</h4>
                  <button data-toggle="modal" id="" data-target="#submitTicket" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Submit Ticket</button>
                </div>
                    
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="example" class="table">
                      <thead class=" text-primary">
                        <tr>
                            <th width="5"><center><b>No</b></center></th>                  
                            <th><center><b>Biro Tujuan</b></center></th>                  
                            <th><center><b>Kategori</b></center></th>                  
                            <th><center><b>Priority</b></center></th>                  
                            <th><center><b>Tanggal Open</b></center></th>                  
                            <th><center><b>Tanggal Close</b></center></th>                  
                            <th><center><b>Status</b></center></th>                  
                            <th><center><b>Aksi</b></center></th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                       
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="submitTicket" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Submit Ticket</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                        <div class="row">
                          <input type="hidden" name="id_user" id="id_user" value="<?= $this->session->userdata('id');?>">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Biro Tujuan</label>
                                    <select class="form-control border-input" name="id_biro" required id="id_biro">
                                      <option value="">-- Pilih Biro Tujuan --</option>
                                     
                                    </select>
                                 </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kategori</label>
                                    <select class="form-control border-input" name="id_kategori" required id="id_kategori">
                                      <option value="">-- Pilih Kategori --</option>
                                    </select>
                                 </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Priority</label>
                                    <select class="form-control border-input" name="priority" required id="priority">
                                      <option value="">-- Pilih Priority --</option>
                                      <option value="low">Low</option>
                                      <option value="normal">Normal</option>
                                      <option value="urgent">Urgent</option>
                                    </select>
                                 </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan</label>
                                    <textarea class="form-control border-input" name="pesan" id="pesan" cols="30" rows="10"></textarea>
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_submit" type="" class="btn btn-success btn-fill btn-wd">
                                Submit Ticket
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div id="ticketMessage" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Ticket Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                    <div class="col-md-12">
                                <div class="form-group" id="modal_body">
                <div class>
                        <div class="row">
                          <input type="hidden" name="id_user" id="id_user" value="<?= $this->session->userdata('id');?>">
                          <input type="hidden" name="id_ticket_input" id="id_ticket_input" value="<?= $this->session->userdata('id');?>">
                            <div class="col-md-12">
                                <div class="form-group" id="body_modal">
                                 </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pesan</label>
                                    <textarea class="form-control border-input" name="pesan" id="pesan_input" cols="30" rows="10"></textarea>
                                 </div>
                            </div>
                        </div>
                    
                    <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_submit_message" type="" class="btn btn-success btn-fill btn-wd">
                                Kirim Pesan
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <script>
         $(document).on('click', '#btn_submit_message' ,function(){
                var id_user                     = $('#id_user').val();
                var pesan                       = $('#pesan_input').val();
                var id_ticket                   = $('#id_ticket_input').val();

            
                $.post("http://localhost/winnerhelpdesk/index.php/WEB/MessageUsers", {
                    id_user                     : id_user,
                    pesan                       : pesan,
                    id_ticket                   : id_ticket,
                    },
                    function(data, status){
                    if (status) {
                        toastr.success('Submit Pesan Success')
                        clearTable()
                        $('#ticketMessage').modal('hide')
                        // clearModalAdd()
                        getTickets()
                    }
                    else {
                        toastr.error('Submit Pesan Failed!')
                    }
                });
                
            });

            $('#id_biro').on('change', function() {
              console.log($('#id_biro').val());
            $.ajax({
              type: "POST",
              data: { id_biro: $('#id_biro').val() },
              url: '<?php echo base_url()."index.php/users/getCategory" ?>',
              dataType: 'text',
              success: function(resp) {
                var json = JSON.parse(resp.replace(',.', ''))
                var $el = $("#id_kategori");
                $el.empty(); // remove old options
                $el.append($("<option></option>")
                .attr("value", '').text('-- Pilih Kategori --'));
                $.each(json, function(key, value) {
                  $el.append($("<option></option>")
                  .attr("value", value.id).text(value.nama_kategori));
                });
              },
              error: function (jqXHR, exception) {
                console.log(jqXHR, exception)
              }
            });
          });
        </script>
      <script>
      function getTickets() {
          var id_user = $('#id_user').val();
            $.get("http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers/" + id_user, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    if (element.tanggal_close == null) {
                      var tgl_close = '-'
                    } else {
                      var tgl_close = element.tanggal_close
                    }
                    if(element.status_ticket == 'open') {
                      t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nama_biro+"</center>",
                          "<center>"+element.nama_kategori+"</center>",
                          "<center>"+element.priority+"</center>",
                          "<center>"+element.tanggal_open+"</center>",
                          "<center>"+tgl_close+"</center>",
                          "<center><button class='btn btn-sm btn-info'>Open</button></center>",
                          "<center><a id='btn_message' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-primary' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-pencil'></i></a></center>"
                      ] ).draw( false );
                      } else if (element.status_ticket == 'work_progress') {
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nama_biro+"</center>",
                          "<center>"+element.nama_kategori+"</center>",
                          "<center>"+element.priority+"</center>",
                          "<center>"+element.tanggal_open+"</center>",
                          "<center>"+tgl_close+"</center>",
                          "<center><button class='btn btn-sm btn-success'>Work Progress</button></center>",
                          "<center><a id='btn_message' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-primary' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-pencil'></i></a></center>"
                      ] ).draw( false );
                      } else if (element.status_ticket == 'closed') { 
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nama_biro+"</center>",
                          "<center>"+element.nama_kategori+"</center>",
                          "<center>"+element.priority+"</center>",
                          "<center>"+element.tanggal_open+"</center>",
                          "<center>"+tgl_close+"</center>",
                          "<center><button class='btn btn-sm btn-warning'>Closed</button></center>",
                          "<center><a id='btn_message' data-idmessage="+element.id_ticket+" class='btn btn-sm btn-primary' data-toggle='modal' data-target='#ticketMessage'><i class='fa fa-pencil'></i></a></center>"
                      ] ).draw( false );
                      } else { 
                        t.row.add( [
                          "<center>"+no+"</center>",
                          "<center>"+element.nama_biro+"</center>",
                          "<center>"+element.nama_kategori+"</center>",
                          "<center>"+element.priority+"</center>",
                          "<center>"+element.tanggal_open+"</center>",
                          "<center>"+tgl_close+"</center>",
                          "<center><button class='btn btn-sm btn-danger'>Done</button></center>",
                          "<center><a id='btn_delete' data-id="+element.id_ticket+" class='btn btn-sm btn-dark'><i class='fa fa-trash'></i></a></center>"
                      ] ).draw( false );
                      }
                    });
                
                } else {
                    alert('Load Data Failed')
                }
            });

            $.get("http://localhost/winnerhelpdesk/index.php/WEB/Biro", function(data, status){
                console.log(data, status)
                if (status) {
                    // data.forEach(element => {
                    var $el = $("#id_biro");
                    // var $ej = $("#id_biro_u");
                        $el.empty(); // remove old options
                            // $ej.empty(); // remove old options
                            $el.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Biro --'));
                        // $ej.append($("<option></option>")
                        // .attr("value", '').text('-- Pilih Biro --'));
                        $.each(data, function(key, value) {
                            console.log(value)
                            $el.append($("<option></option>")
                            .attr("value", value.id).text(value.nama_biro));
                            // $ej.append($("<option></option>")
                            // .attr("value", value.id).text(value.nama_biro));
                        });
                    // });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

            function clearTable() {
                var table = $('#example').DataTable();
                table
                    .clear()
                    .draw();
            }
            

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {

            $('#example').DataTable( {
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );

            getTickets()

        } );

            $(document).on('click', '#btn_submit' ,function(){
                var id_user                         = $('#id_user').val();
                var id_biro                         = $('#id_biro').val(); 
                var id_kategori                     = $('#id_kategori').val(); 
                var priority                        = $('#priority').val(); 
                var pesan                           = $('#pesan').val(); 
                console.log(id_biro)

            
                $.post("http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers", {
                    id_user                         : id_user,
                    id_biro                         : id_biro,
                    id_kategori                     : id_kategori,
                    priority                        : priority,
                    pesan                           : pesan,
                    },
                    function(data, status){
                    if (status) {
                        toastr.success('Submit Ticket Success')
                        clearTable()
                        $('#submitTicket').modal('hide')
                        location.reload();
                        getTickets()
                    }
                    else {
                        toastr.error('Submit Ticket Failed!')
                    }
                });
                
            });

            $(document).on('click', '#btn_message' ,function(){
                  var $el = $("#body_modal");
                  $el.empty(); // remove old options
                // clearModalMessage()
                var id =  $(this).data("idmessage");
                $("#id_ticket_input").val(id)
                $.ajax({
                type: "POST",
                data: { id_ticket: id },
                url: '<?php echo base_url()."index.php/users/getMessage" ?>',
                dataType: 'text',
                success: function(resp) {
                  console.log(resp)
                  var json = JSON.parse(resp.replace(',.', ''))
                  var $el = $("#body_modal");
                  // $el.empty(); // remove old options
                  $.each(json, function(key, value) {
                    if (value.nama_user == null) {
                    $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_staff+ '-'+ value.tanggal));
                    } else {
                    $el.append($("<textarea readonly class='form-control border-input'></textarea><br>").text(value.pesan + ' //' + value.nama_user+ '-'+ value.tanggal));
                    }
                  });
                },
                error: function (jqXHR, exception) {
                  console.log(jqXHR, exception)
                }
              });
            });

            $(document).on('click', '#btn_done' ,function(){
                var id =  $(this).data("iddone");
                console.log(id)
                var data = {
                    status : 'done',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Update status ticket success!')
                    location.reload();
                    }
                });

            });

            $(document).on('click', '#btn_delete' ,function(){
                var id =  $(this).data("id");
                console.log(id)
                var data = {
                    status : 'deleted',
                    }

                jQuery.ajax({
                url: 'http://localhost/winnerhelpdesk/index.php/WEB/TicketsUsers/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()  
                    getTickets();
                    toastr.success('Delete ticket success!')
                    location.reload();
                    }
                });

            });
            // $(document).on('click', '#btn_status' ,function(){
            //     var id =  $(this).data("idstatus");
            //     console.log(id)
            //     var data = {
            //         status : closed,
            //         }

            //     jQuery.ajax({
            //     url: 'http://localhost/winnerhelpdesk/index.php/WEB/Tickets/' + id,
            //     type: 'PUT',
            //     data : data,
            //     success: function(data) {
            //         $('#editTickets').modal('hide')
            //         clearModalEdit()
            //         clearTable()
            //         getTickets();
            //         toastr.success('Update Data Success!')
            //         }
            //     });

            // });
           

        
      </script>