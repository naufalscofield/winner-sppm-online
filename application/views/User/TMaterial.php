<div class="main-panel">
    <!-- Navbar -->

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">Transaksi Material - Judul</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Material Untuk <b><?= $judul->judul; ?></b></h4>
                            
                            <button data-toggle="modal" id="addUser" data-target="#addMaterial" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Material</button>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th>
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>Nama Material</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addMaterial" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Material Ke Judul</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="material_input">
                                <label>Nama Material</label>
                                <input type="hidden" id="id_judul" value="<?=$judul->id;?>">
                                <select class="form-control" id="material" name="material[]" multiple="multiple" style="width: 75%">
                                <?php foreach ($material as $material){ ?>
                                    <option value="<?= $material['id'];?>"><?= $material['material'];?></option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                            Tambah Material
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="editJudul" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit dan Detail Judul</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="judul_edit">
                                <label for="">Judul SPPM</label>
                                <input required type="hidden" name="id_u" id="id_u" class="form-control">
                                <input required type="text" name="judul_u" id="judul_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                            Perbarui Judul
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>

    <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
    <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
    <script>
    function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterRole");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[6];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
      function getTMaterial() {
          var id_judul = $('#id_judul').val();
            $.get("http://localhost/winnersppmonline/index.php/WEB/TMaterial/" + id_judul , function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.material+"</center>",
                        "<center><button type='button' id='btn_delete' data-id="+element.idtmaterial+" class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Material Dari Judul Ini'><i class='fa fa-trash'></i></button></center>"
                    ] ).draw( false );
                   
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

          function clearTable() 
          {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
            }
        function clearModalAdd()
            {
                        $("#judul").val('')
            }

            function clearModalDetail()
            {
                        $("#judul_u").val('')
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('#material').select2();
            $('#example').DataTable( {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                dom: 'lBfrtip',
            } );

            getTMaterial()

        } );

            $(document).on('click', '#btn_add' ,function(){
                // console.log('aw')
                var id_judul               = $('#id_judul').val();
                var material               = $('#material').val();

                            if (material == '') {
                                toastr.warning('Harap pilih material!')
                                } else {
                                    $.post("http://localhost/winnersppmonline/index.php/WEB/TMaterial", {
                                        id_judul             : id_judul,
                                        material             : material,
                                        },
                                        function(data, status){
                                        if (status) {
                                            toastr.success('Material berhasil ditambahkan!')
                                            clearTable()
                                            $('#addMaterial').modal('hide')
                                            clearModalAdd()
                                            getTMaterial()
                                        }
                                        else {
                                            toastr.error('Material gagal ditambahkan!')
                                        }
                                    });
                                }
                            });         

            $(document).on('click', '#btn_edit' ,function(){
                clearModalDetail()
                $('#editJudul').modal('show')
                var id =  $(this).data("id");
                console.log(id)
                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnersppmonline/index.php/WEB/TMaterial/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)
                        $("#judul_u").val(resp.judul)
                        $("#id_u").val(id)

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                      = $("#id_u").val();
                var judul                   = $("#judul_u").val();
                console.log(status)

                if (judul == '')
                {
                    toastr.error('Harap lengkapi data!')
                } else {
                    var data = {
                        judul : judul,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/TMaterial/' + id,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#editJudul').modal('hide')
                        clearModalDetail()
                        clearTable()
                        getTMaterial();
                        toastr.success('Sukses Perbarui Judul!')
                        }
                    });
                }
            });
           
            $(document).on('click', '#btn_delete' ,function(){
                var id                      = $(this).data("id");
                // console.log(status)

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/TMaterial/' + id,
                    type: 'delete',
                    success: function() {
                        clearTable()
                        getTMaterial();
                        toastr.success('Material sukses dihapus!')
                        }
                    });
            });
            $(document).on('click', '#btn_back', function(){
              window.history.back();
            })
      </script>