<div class="main-panel">

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">
                <input type="hidden" value="<?= $sppm->id;?>" id="id_sppm"></a>
                <input type="hidden" value="<?= $this->session->userdata('role');?>" id="role_session"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Daftar Material Untuk SPPM No <b><?= $sppm->no_sppm;?></b></h4><br>
                            <h5 class="card-title "><b><?= $sppm->jenis_sppm. ' ' .$sppm->keterangan;?></b></h5><br>
                            <button data-toggle="modal" id="" data-target="#addMaterial" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah material untuk SPPM ini</button>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th width="5">
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>Material</b></center>
                                            </th>
                                            <th width="20">
                                                <center><b>Merek</b></center>
                                            </th>
                                            <th>
                                                <center><b>Spesifikasi Teknik</b></center>
                                            </th>
                                            <th>
                                                <center><b>Qty</b></center>
                                            </th>
                                            <th>
                                                <center><b>Satuan</b></center>
                                            </th>
                                            <th>
                                                <center><b>Harga</b></center>
                                            </th>
                                            <th>
                                                <center><b>File</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addMaterial" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Material Ke Judul</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" enctype="multipart/form-data"></form>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="material_input">
                                    <label>Nama Material</label>
                                    <input type="text" id="material" name="material" class="form-control">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="merek_input">
                                    <label>Merek</label>
                                    <input type="text" id="merek" name="merek" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="harga_input">
                                    <label>Harga</label>
                                    <input type="text" id="harga" name="harga" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="qty_input">
                                    <label>QTY</label>
                                    <input type="text" id="qty" name="qty" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="satuan_input">
                                    <label>Satuan</label>
                                    <input type="text" id="satuan" name="satuan" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="spesifikasi_input">
                                    <label>Spesifikasi Teknik</label>
                                    <input type="text" id="spesifikasi_teknik" name="spesifikasi_teknik" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <label>File</label>
                                <input type="file" id="file" name="file" class="form-control">
                            </div>
                        </div> -->

                        <div class="clearfix">
                        </div>
                        <div class="modal-footer">
                            <button id="btn_add" type="button" class="btn btn-success btn-fill btn-wd">
                                Tambah Material
                            </button>

                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div id="attachFile" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Attach File Untuk Material Ini</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data" action="<?= base_url();?>index.php/web/SPPMMat2_File">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="uri" id="uri" value="<?php echo $this->uri->segment(3);?>" class="form-control">
                                <input type="hidden" id="id_sppm_mat" name="id_sppm_mat">
                                <label for="">File</label>
                                <input type="file" name="file_u" id="file_u" class="form-control">
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div class="modal-footer">
                            <button id="btn_aksi_attach" type="submit" class="btn btn-success btn-fill btn-wd">
                                Attach
                            </button>

                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
    <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
    <script>
    function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterRole");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[6];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
      function getSPPMMat() {
          var id_sppm = $('#id_sppm').val();
            $.get("http://localhost/winnersppmonline/index.php/WEB/SPPMMat2/" + id_sppm, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    if(element.file !== null ){
                        var file = 'file'
                    } else {
                        var file = ''
                    }
                    if($('#role_session').val() !== 'pengadaan') {
                        t.row.add( [
                            "<center>"+no+"</center>",
                            "<center>"+element.material_mat+"</center>",
                            "<center>"+(element.merek || '-')+"</center>",
                            "<center>"+(element.spesifikasi_teknik_mat || '-')+"</center>",
                            "<center>"+(element.qty || '-')+"</center>",
                            "<center>"+(element.satuan || '-')+"</center>",
                            "<center>"+(element.harga || '-')+"</center>",
                            "<center><a href = '<?=base_url();?>assets/file_spesifikasi_teknik_material/"+element.file+"' target='_new'>"+file+"</a></center>",
                            "<center><button type='button' id='btn_hapus' data-id="+element.id+" class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Material Dari SPPM Ini'><i class='fa fa-trash'></i></button><button type='button' id='btn_attach' data-idattach="+element.id+" class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Attach file'><i class='fa fa-paperclip'></i></button></center>"
                        ] ).draw( false );
                    } else {
                        t.row.add( [
                            "<center>"+no+"</center>",
                            "<center>"+element.material_mat+"</center>",
                            "<center>"+(element.merek || '-')+"</center>",
                            "<center>"+(element.spesifikasi_teknik_mat || '-')+"</center>",
                            "<center>"+(element.qty || '-')+"</center>",
                            "<center>"+(element.satuan || '-')+"</center>",
                            "<center>"+(element.harga || '-')+"</center>",
                            "<center><a href = '<?=base_url();?>assets/file_spesifikasi_teknik_material/"+element.file+"' target='_new'>"+file+"</a></center>",
                            "<center></center>"
                        ] ).draw( false );
                    }
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

          function clearTable() 
            {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
            }
          
          function clearTable2() 
            {
            var table = $('#example2').DataTable();
             table
                .clear()
                .draw();
            }

            function clearModalDetail()
            {
                        $("#material_u").val('')
                        $("#merek_u").val('')
                        $("#spesifikasi_teknik_u").val('')
                        $("#qty_u").val('')
                        $("#satuan_u").val('')
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });


            $('#example').DataTable( {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "columnDefs": [
                    { "width": "20%", "targets": 6 },
                    { "width": "30%", "targets": 3 },
                    { "width": "30%", "targets": 8 },
                    { "width": "20%", "targets": 2 },
                ],
                dom: 'lBfrtip',
            } );

            getSPPMMat()      
        } );        

            $(document).on('click', '#btn_hapus' ,function(){
                var id = $(this).data("id");
                jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/SPPMMat2/' + id,
                    type: 'delete',
                    success: function(data) {
                        clearTable()
                        getSPPMMat();
                        toastr.success('Material dihapus!')
                        }
                    });
            });
            
            $(document).on('click', '#btn_attach' ,function(){
                $('#attachFile').modal('show')
                var id_sppm_mat = $(this).data("idattach")
                $('#id_sppm_mat').val(id_sppm_mat)
            });


            $(document).on('click', '#btn_add' ,function(){
                var id_sppm                 = $("#id_sppm").val();
                var material_mat            = $("#material").val();
                var merek                   = $("#merek").val();
                var harga                   = $("#harga").val();
                var qty                     = $("#qty").val();
                var satuan                  = $("#satuan").val();
                var spesifikasi_teknik_mat  = $("#spesifikasi_teknik").val();

                
                $.post("http://localhost/winnersppmonline/index.php/WEB/SPPMMat2", {
                    id_sppm                : id_sppm,
                    material_mat           : material_mat,
                    merek                  : merek,
                    harga                  : harga,
                    qty                    : qty,
                    satuan                 : satuan,
                    spesifikasi_teknik_mat : spesifikasi_teknik_mat,
                },
                    function(data, status){
                    if (status) {
                        toastr.success('Material sukses ditambah!')
                        clearTable()
                        $('#addMaterial').modal('hide')
                        clearModalAdd()
                        getSPPMMat()
                    }
                    else {
                        toastr.error('Material gagal ditambahkan!')
                    }
                });
            });

            function clearModalAdd()
            {
                $("#material_mat").val('')
                $("#merek").val('')
                $("#harga").val('')
                $("#satuan").val('')
                $("#qty").val('')
                $("#spesifikasi_teknik_mat").val('')
                $("#file").val('')
            }

            $(document).on('click', '#btn_back', function(){
  window.history.back();
})

      </script>