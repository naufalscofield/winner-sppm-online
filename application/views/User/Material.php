<div class="main-panel">
    <!-- Navbar -->

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">Material</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Daftar Material</h4>
                            <button data-toggle="modal" id="" data-target="#addMaterial" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Material</button>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th width="5">
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>Kode Material</b></center>
                                            </th>
                                            <th>
                                                <center><b>Nama Material</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addMaterial" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Buat Material Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="kode_material_input">
                                <label>Kode Material</label><br>
                                <!-- <input required type="text" name="kode_material" id="kode_material" class="form-control"> -->
                                <select required style="width: 100%" class="form-control" id="kode_material">
                                
                                    <?php 
                                    $q = $this->db->get('nomenklatur')->result_array();
                                    foreach($q as $q){ 
                                    if ($q['lv1'] == null){
                                        $lv1 = '';
                                    } else {
                                        $lv1 = $q['lv1'];
                                    }

                                    if ($q['lv2'] == null){
                                        $lv2 = '';
                                    } else {
                                        $lv2 = $q['lv2'];
                                    }

                                    if ($q['lv3'] == null){
                                        $lv3 = '';
                                    } else {
                                        $lv3 = $q['lv3'];
                                    }

                                    if ($q['lv4'] == null){
                                        $lv4 = '';
                                    } else {
                                        $lv4 = $q['lv4'];
                                    }

                                    if ($q['lv5'] == null){
                                        $lv5 = '';
                                    } else {
                                        $lv5 = $q['lv5'];
                                    }

                                    ?>
                                    <option value="<?= $q['id'];?>"><?= $lv1.$lv2.$lv3.$lv4.$lv5.'-'.$q['keterangan'];?></option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="material_input">
                                <label>Nama Material</label>
                                <input required type="text" name="material" id="material" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                            Buat Material Baru
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="editMaterial" class="modal" role="dialog">
        <div class="modal-dialog modal-lm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit dan Detail Material</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="kode_material_edit">
                                <label for="">Kode Material</label>
                                <!-- <input required type="text" name="kode_material_u" id="kode_material_u" class="form-control"> -->
                                <select required style="width: 100%" class="form-control" id="kode_material_u">
                                <option class="form-control" value="">--Pilih--</option>
                                    <?php 
                                    $q = $this->db->get('nomenklatur')->result_array();
                                    foreach($q as $q){ 
                                    if ($q['lv1'] == null){
                                        $lv1 = '';
                                    } else {
                                        $lv1 = $q['lv1'];
                                    }

                                    if ($q['lv2'] == null){
                                        $lv2 = '';
                                    } else {
                                        $lv2 = $q['lv2'];
                                    }

                                    if ($q['lv3'] == null){
                                        $lv3 = '';
                                    } else {
                                        $lv3 = $q['lv3'];
                                    }

                                    if ($q['lv4'] == null){
                                        $lv4 = '';
                                    } else {
                                        $lv4 = $q['lv4'];
                                    }

                                    if ($q['lv5'] == null){
                                        $lv5 = '';
                                    } else {
                                        $lv5 = $q['lv5'];
                                    }

                                    ?>
                                    <option value="<?= $q['id'];?>"><?= $lv1.$lv2.$lv3.$lv4.$lv5.'-'.$q['keterangan'];?></option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="material_edit">
                                <label for="">Nama Material</label>
                                <input required type="hidden" name="id_u" id="id_u" class="form-control">
                                <input required type="text" name="material_u" id="material_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                            Perbarui Material
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
    <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
    <script>
    function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterRole");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[6];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
      function getMaterial() {
            $.get("http://localhost/winnersppmonline/index.php/WEB/material", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    if (element.lv1 == null){
                            var lv1 = '';
                        } else {
                            var lv1 = element.lv1;
                        }

                        if (element.lv2 == null){
                            var lv2 = '';
                        } else {
                            var lv2 = element.lv2;
                        }

                        if (element.lv3 == null){
                            var lv3 = '';
                        } else {
                            var lv3 = element.lv3;
                        }

                        if (element.lv4 == null){
                            var lv4 = '';
                        } else {
                            var lv4 = element.lv4;
                        }

                        if (element.lv5 == null){
                            var lv5 = '';
                        } else {
                            var lv5 = element.lv5;
                        }
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+lv1+lv2+lv3+lv4+lv5+"</center>",
                        "<center>"+element.material+"</center>",
                        "<center><button type='button' id='btn_edit' data-id="+element.idmaterial+" class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Detail dan Edit Judul'><i class='fa fa-pencil'></i></button><button type='button' id='btn_delete' data-id="+element.idmaterial+" class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Judul'><i class='fa fa-trash'></i></button></center>"
                    ] ).draw( false );
                   
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

          function clearTable() 
          {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
            }
        function clearModalAdd()
            {
                        $("#material").val('')
            }

            function clearModalDetail()
            {
                        $("#material_u").val('')
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('#kode_material').select2();
            $('#kode_material_u').select2();

            $('#example').DataTable( {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                dom: 'lBfrtip',
            } );

            getMaterial()

        } );

            $(document).on('click', '#btn_add' ,function(){
                // console.log('aw')
                var material               = $('#material').val();
                var kode_material          = $('#kode_material').val();

                            if ( (material == '') || (kode_material == '' )) {
                                toastr.warning('Harap lengkapi form Material!')
                                } else {
                                    $.post("http://localhost/winnersppmonline/index.php/WEB/material", {
                                        material             : material,
                                        kode_material        : kode_material,
                                        },
                                        function(data, status){
                                        if (data.status == true) {
                                            toastr.success(data.message)
                                            clearTable()
                                            $('#addMaterial').modal('hide')
                                            clearModalAdd()
                                            getMaterial()
                                        }
                                        else {
                                            toastr.error(data.message)
                                        }
                                    });
                                }
                            });         

            $(document).on('click', '#btn_edit' ,function(){
                clearModalDetail()
                $('#editMaterial').modal('show')
                var id =  $(this).data("id");
                console.log(id)
                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnersppmonline/index.php/WEB/material/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)
                        $("#kode_material_u").val(resp.kode_material)
                        $("#material_u").val(resp.material)
                        $("#id_u").val(id)

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                      = $("#id_u").val();
                var material                   = $("#material_u").val();
                var kode_material                   = $("#kode_material_u").val();
                console.log(status)

                if ((material == '') || (kode_material == ''))
                {
                    toastr.error('Harap lengkapi data!')
                } else {
                    var data = {
                        material : material,
                        kode_material : kode_material,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/material/' + id,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        if(data.status == true){
                            $('#editMaterial').modal('hide')
                            clearModalDetail()
                            clearTable()
                            getMaterial();
                            toastr.success('Sukses Perbarui Material!')

                            } else {
                                        toastr.error(data.message)
                                    }
                        }
                    });
                }
            });
           
            $(document).on('click', '#btn_delete' ,function(){
                var id                      = $(this).data("id");
                // console.log(status)

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/material/' + id,
                    type: 'delete',
                    success: function() {
                        clearTable()
                        getMaterial();
                        toastr.success('Material sukses dihapus!')
                        }
                    });
            });

            $(document).on('click', '#btn_back', function(){
            window.history.back();
            })

      </script>