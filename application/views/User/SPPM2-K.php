<div class="main-panel">
    <!-- Navbar -->
<style>
.createdDiv {
    
}
</style>
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">SPPM Material</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Daftar SPPM Masuk (Material)</h4>
                            <!-- <button data-toggle="modal" id="addUser" data-target="#addSPPM" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Buat SPPM Baru</button> -->
                            <!-- <a href="#" data-toggle="tooltip" title="Hooray!">Hover over me</a> -->
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th width="5">
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>No SPPM</b></center>
                                            </th>
                                            <th width="20">
                                                <center><b>Judul</b></center>
                                            </th>
                                            <th>
                                                <center><b>Tanggal Dibuat</b></center>
                                            </th>
                                            <th>
                                                <center><b>Dibuat Oleh</b></center>
                                            </th>
                                            <th>
                                                <center><b>Status</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addSPPM" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Buat SPPM Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="no_sppm_input">
                                <label>No SPPM</label>
                                <input required type="text" name="no_sppm" id="no_sppm" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="judul_input">
                                <label>Judul</label>
                                <input required type="text" name="judul" id="judul" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="id_judul_input">
                                <input required type="hidden" name="id_judul" id="id_judul" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="jenis_sppm_input">
                                <label>Jenis</label>
                                <select name="jenis_sppm" id="jenis_sppm" required class="form-control">
                                    <option selected value=""> -- Pilih Jenis --</option>
                                    <option value="SPPM">SPPM</option>
                                    <option value="Revisi SPPM">Revisi SPPM</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="no_spk_input">
                                <label>No SPK</label>
                                <input required type="text" name="no_spk" id="no_spk" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="no_rkm_input">
                                <label>No RKM</label>
                                <input required type="text" name="no_rkm" id="no_rkm" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="forecast_input">
                                <label>Forecast</label>
                                <input required type="text" name="forecast" id="forecast" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="stok_input">
                                <label>Stok</label>
                                <input required type="text" name="stok" id="stok" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="forecast_input">
                                <label>Target Waktu Delivery</label>
                                <input required type="date" name="target_delivery" id="target_delivery" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="id_manager_input">
                                <label>Manager</label>
                                <select name="id_manager" id="id_manager" required class="form-control">
                                    <option selected value=""> -- Pilih Manager --</option>
                                    <?php foreach ($manager as $manager) { ?>
                                        <option value="<?=$manager['id'];?>">
                                            <?=$manager['nm_peg'];?>
                                        </option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="id_koordinator_input">
                                <label>Koordinator</label>
                                <select name="id_koordinator" id="id_koordinator" required class="form-control">
                                    <option selected value=""> -- Pilih Koordinator --</option>
                                    <?php foreach ($koordinator as $koordinator) { ?>
                                        <option value="<?=$koordinator['id'];?>">
                                            <?=$koordinator['nm_peg'];?>
                                        </option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                            Buat SPPM
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="detailSPPM" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail SPPM</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="no_sppm_edit">
                                <label for="">No SPPM</label>
                                <input required readonly type="text" name="no_sppm_u" id="no_sppm_u" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="judul_edit">
                                <label for="">Judul SPPM</label>
                                <input required readonly type="text" name="judul_u" id="judul_u" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="no_spk_edit">
                                <label for="">No SPK</label>
                                <input required readonly type="text" name="no_spk_u" id="no_spk_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="no_rkm_edit">
                                <label for="">No RKM</label>
                                <textarea required readonly name="no_rkm_u" id="no_rkm_u" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="forecast_edit">
                                <label for="">Forecast</label>
                                <input required readonly type="text" name="forecast_u" id="forecast_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="stok_edit">
                                <label for="">Stok</label>
                                <input required readonly type="text" name="stok_u" id="stok_u" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="total_pemesanan_edit">
                                <label for="">Total Pemesanan</label>
                                <input required readonly type="text" name="total_pemesanan_u" id="total_pemesanan_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="tanggal_dibuat_edit">
                                <label for="">Tanggal Dibuat</label>
                                <input required readonly type="text" name="tanggal_dibuat_u" id="tanggal_dibuat_u" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="nm_peg_edit">
                                <label for="">Dibuat Oleh</label>
                                <input required readonly type="text" name="nm_peg_u" id="nm_peg_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="manager_edit">
                                <label for="">Manager</label>
                                <input required readonly type="text" name="manager_u" id="manager_u" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="koordinator_edit">
                                <label for="">Koordinator</label>
                                <input required readonly type="text" name="koordinator_u" id="koordinator_u" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="staf_pengadaan_edit">
                                <label for="">Staf Pengadaan</label>
                                <input required readonly type="text" name="staf_pengadaan_u" id="staf_pengadaan_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="target_delivery_edit">
                                <label for="">Target Delivery</label>
                                <input required readonly type="text" name="target_delivery_u" id="target_delivery_u" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="konfirmasiPassword" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Konfirmasi Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4><b>Pastikan ini anda, <?= $this->session->userdata('nm_peg');?></b></h4>
                            <hr>
                            <div class="form-group" id="password_edit">
                                <label for="">Password</label>
                                <input required readonly type="hidden" name="id_sppm_u" id="id_sppm_u" class="form-control">
                                <input required readonly type="hidden" name="id_user_u" value="<?= $this->session->userdata('id');?>" id="id_user_u" class="form-control">
                                <input required type="password" name="password_u" id="password_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_konfirmasi" type="" class="btn btn-success btn-fill btn-wd">
                            Konfirmasi Password
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div id="tabelJudulSPPM" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Judul SPPM</h5>
                        <h6 id="list_nama" class="modal-title">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="table-responsive ">
                        <table id="example2" class="table">
                            <thead>
                                <tr>
                                    <center><th width=""><b>Judul SPPM</b></th></center>
                                    <center><th><b></b></th></center>
                                </tr>
                            </thead>
                            <tbody id="table-row">
                                    <?php foreach($judul as $judul){ ?>
                                <tr>
                                    <center><td id="" width=""> <?= $judul['judul'];?> </td></center>
                                    <center><td> <button class='btn btn-sm btn-success' id='btn_pilihJudul' data-nama_judul='<?=$judul['judul'];?>' data-id_judul='<?=$judul['id'];?>'><i class='fa fa-check'></i></button> </td></center>
                                </tr>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
    <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
    <script>
    function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterRole");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[6];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
      function getSPPM() {
            $.get("http://localhost/winnersppmonline/index.php/WEB/SPPM2", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.no_sppm+"</center>",
                        "<center>"+element.jenis_sppm+' '+element.keterangan+"</center>",
                        "<center>"+element.tanggal_dibuat+"</center>",
                        "<center>"+element.nm_peg+"</center>",
                        "<center><button type='button' id='btn_acc_1' class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Sudah Acc Peminta'><i class='fa fa-check'></i> Acc Peminta</button></center>",
                        "<center><button type='button' id='btn_acc_koordinator' data-id="+element.idsppm+" class='btn btn-sm btn-success' data-toggle='tooltip' data-placement='top' title='Acc'><i class='fa fa-check'></i></button><button type='button' id='btn_detail_sppm' data-iddetail="+element.idsppm+" class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Detail SPPM'><i class='fa fa-pencil'></i></button><a href='<?=base_url();?>index.php/user/getSPPMMat2/"+element.idsppm+"' id='btn_sppmmaterial' class='btn btn-sm btn-primary' data-toggle='tooltip' data-placement='top' title='Daftar Material'><i class='fa fa-cubes'></i></a></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

          function clearTable() 
          {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
            }
        function clearModalAdd()
            {
                        $("#no_sppm").val('')
                        $("#judul").val('')
                        $("#jenis_sppm").val('')
                        $("#no_spk").val('')
                        $("#no_rkm").val('')
                        $("#forecast").val('')
                        $("#stok").val('')
                        $("#total_pemesanan").val('')
                        $("#tanggal_dibuat").val('')
                        $("#id_peminta").val('')
                        $("#id_manager").val('')
                        $("#id_koordinator").val('')
                        $("#id_staf_pengadaan").val('')
                        $("#target_delivery").val('')
            }

            function clearModalDetail()
            {
                        $("#nip_u").val('')
                        $("#nm_peg_u").val('')
                        $("#status_u").val('')
                        $("#jns_kelamin_peg_u").val('')
                        $("#alamat_u").val('')
                        $("#kodepos_u").val('')
                        $("#telepon_u").val('')
                        $("#handphone_u").val('')
                        $("#email_u").val('')
                        $("#kd_jabatan_u").val('')
                        $("#nm_jabatan_u").val('')
                        $("#kd_unit_org_u").val('')
                        $("#nm_unit_org_u").val('')
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('body').tooltip({
                selector: '[data-toggle=tooltip]'
            });
            // $('[data-toggle="tooltip"]').tooltip();

            $(document).on('click', '#judul' ,function(){
                $('#tabelJudulSPPM').modal('show')
            }),
            $(document).on('focus', '#judul' ,function(){
                $('#tabelJudulSPPM').modal('show')
            }),

            $('#example').DataTable( {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "columnDefs": [
                    { "width": "25%", "targets": 2 },
                    // { "width": "22%", "targets": 3 },
                    // { "width": "30%", "targets": 4 },
                    { "width": "22%", "targets": 6 },
                ],
                dom: 'lBfrtip',
            } );

            $('#example2').DataTable( {
                dom: 'Bfrtip',
            } );

            getSPPM()

        } );

            $(document).on('click', '#btn_pilihJudul', function(){
                var id_judul = $(this).data("id_judul");
                var nama_judul = $(this).data("nama_judul");
                $('#judul').val(nama_judul);
                $('#id_judul').val(id_judul);
                $('#tabelJudulSPPM').modal('hide');
            })

            $(document).on('click', '#btn_add' ,function(){
                // console.log('aw')
                var no_sppm             = $('#no_sppm').val();
                var judul               = $('#judul').val();
                var id_judul            = $('#id_judul').val();
                var jenis_sppm          = $('#jenis_sppm').val();
                var no_spk              = $('#no_spk').val();
                var no_rkm              = $('#no_rkm').val();
                var forecast            = $('#forecast').val();
                var stok                = $('#stok').val();
                var id_manager          = $('#id_manager').val();
                var id_koordinator      = $('#id_koordinator').val();
                var target_delivery     = $('#target_delivery').val();

                            if ( (judul == '') || (id_manager == '') || (id_koordinator == '')  ) {
                                toastr.warning('Harap lengkapi form SPPM!')
                                } else {
                                    $.post("http://localhost/winnersppmonline/index.php/WEB/SPPM2", {
                                        no_sppm             : no_sppm,
                                        id_judul            : id_judul,
                                        jenis_sppm          : jenis_sppm,
                                        no_spk              : no_spk,
                                        no_rkm              : no_rkm,
                                        forecast            : forecast,
                                        stok                : stok,
                                        id_manager          : id_manager,
                                        id_koordinator      : id_koordinator,
                                        target_delivery     : target_delivery,
                                        },
                                        function(data, status){
                                        if (status) {
                                            toastr.success('SPPM Sukses Dibuat!')
                                            clearTable()
                                            $('#addSPPM').modal('hide')
                                            clearModalAdd()
                                            getSPPM()
                                        }
                                        else {
                                            toastr.error('SPPM Gagal Dibuat!')
                                        }
                                    });
                                }
                            });         

            $(document).on('click', '#btn_acc_koordinator' ,function(){
               $('#konfirmasiPassword').modal('show');
                var id = $(this).data("id");
                $('#id_sppm_u').val(id)
            
            });

            $(document).on('click', '#btn_konfirmasi' ,function(){
                var id_user = $('#id_user_u').val();
                var password = $('#password_u').val();

                if (password == '') {
                    toastr.warning('Isi password!');
                } else {
                $.post("http://localhost/winnersppmonline/index.php/WEB/CekPassword", {
                        id                  : id_user,
                        password            : password,
                    
                        },
                        function(data, status){
                        if (data.status == true) {
                            toastr.success(data.message)
                            $('#konfirmasiPassword').modal('hide')
                            var data = {
                            status : 'acc_koordinator'
                            }
                            var id = $('#id_sppm_u').val();
                            jQuery.ajax({
                                url: 'http://localhost/winnersppmonline/index.php/WEB/sppm2/' + id,
                                type: 'PUT',
                                data : data,
                                success: function(data) {
                                    clearTable()
                                    getSPPM();
                                    toastr.success('Update Status SPPM Sukses!')
                                    // location.reload()
                                    }
                                });
                        }
                        else {
                            toastr.error(data.message)
                        }
                    });
                }
            });

            $(document).on('click', '#btn_detail_sppm' ,function(){
                clearModalDetail()
                $('#detailSPPM').modal('show')
                var id =  $(this).data("iddetail");
                console.log(id)
                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnersppmonline/index.php/WEB/SPPM2/' + id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)
                    if(resp.nama_staf_pengadaan  == null) {
                        var sp = 'Belum Ada'
                    } else {
                        var sp = resp.nama_staf_pengadaan
                    }
                        $("#no_sppm_u").val(resp.no_sppm)
                        $("#judul_u").val(resp.jenis_sppm + ' ' + resp.judul)
                        $("#no_spk_u").val(resp.no_spk)
                        $("#no_rkm_u").val(resp.no_rkm)
                        $("#forecast_u").val(resp.forecast)
                        $("#stok_u").val(resp.stok)
                        $("#stok_pemesanan_u").val(resp.stok_pemesanan)
                        $("#tanggal_dibuat_u").val(resp.tanggal_dibuat)
                        $("#nm_peg_u").val(resp.nama_peminta)
                        $("#target_delivery_u").val(resp.target_delivery)
                        $("#manager_u").val(resp.nama_manager)
                        $("#koordinator_u").val(resp.nama_koordinator)
                        $("#staf_pengadaan_u").val(sp)

                    },
                    error: function (jqXHR, exception) {
                    // console.log(jqXHR, exception)
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var nip                 = $("#nip_u").val();
                var status              = $("#status_u").val();
                var role                = $("#role_u").val();
                var id_biro             = $("#id_biro_u").val();
                console.log(status)

                if ( (nip == '') || (status == '') || (role == '') || (id_biro == '') )
                {
                    toastr.error('Harap lengkapi data!')
                } else {
                    var data = {
                        status : status,
                        role : role,
                        id_biro : id_biro,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/SPPM2/' + nip,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#detailSPPM').modal('hide')
                        clearModalDetail()
                        clearTable()
                        getSPPM();
                        toastr.success('Update Data Success!')
                        }
                    });
                }
            });

            $(document).on('click', '#btn_back', function(){
  window.history.back();
})

      </script>