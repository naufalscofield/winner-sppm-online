<?php $myData = $material; ?>
<div class="main-panel">
    <!-- Navbar -->

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">Spesifikasi Teknik</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Spesifikasi Teknik</h4>
                            <button data-toggle="modal" id="addUser" data-target="#addSpesifikasi" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Spesifikasi Teknik</button>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th width="5">
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>Material</b></center>
                                            </th>
                                            <th>
                                                <center><b>Spesifikasi Teknik</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addSpesifikasi" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Buat Spesifikasi Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group " id="id_material_input">
                                <label>Material</label><br>   
                                    <select class="js-example-basic-single form-control" id="id_material" name="id_material" style="width: 100%">
                                    <?php 
                                    foreach ($myData as $material){ ?>
                                        <option value="<?= $material['id'];?>"><?= $material['material'];?></option>
                                    <?php } ?>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="spesifikasi_input">
                                <label>Spesifikasi</label>
                                <textarea class="form-control" name="spesifikasi" id="spesifikasi"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                            Buat Spesifikasi Baru
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="editSpesifikasi" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit dan Spesifikasi Teknik</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="id_material_edit">
                                <input required type="hidden" name="id_u" id="id_u" class="form-control">

                                <label>Material</label><br>   
                                <select disabled class="form-control border-input" id="id_material_u" name="id_material_u" style="width: 100%">
                                <option value="">--</option>
                                    <?php 
                                    foreach ($myData as $material2){ ?>
                                        <option value="<?= $material2['id'];?>"><?= $material2['material'];?></option>
                                    <?php } ?>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="spesifikasi_edit">
                                <label for="">Spesifikasi</label>
                                <textarea class="form-control" id="spesifikasi_u" name="spesifikasi_u"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                            Perbarui Spesifikasi
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>

    
    
    <script src="<?=base_url();?>assets/ladda/dist/spin.min.js"></script>
    <script src="<?=base_url();?>assets/ladda/dist/ladda.min.js"></script>
    <script>
    function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("filterRole");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[6];
                    if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        }
                    }
                }
            }
      function getSpesifikasi() {
            $.get("http://localhost/winnersppmonline/index.php/WEB/spesifikasi", function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.material+"</center>",
                        "<center>"+element.spesifikasi_teknik+"</center>",
                        "<center><button type='button' id='btn_edit' data-id="+element.idspesifikasi+" class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Detail dan Edit Judul'><i class='fa fa-pencil'></i></button><button type='button' id='btn_delete' data-id="+element.idspesifikasi+" class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Judul'><i class='fa fa-trash'></i></button></center>"
                    ] ).draw( false );
                   
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

          function clearTable() 
          {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
            }
        function clearModalAdd()
            {
                        $("#id_material").val('')
                        $("#spesifikasi").val('')
            }

            function clearModalDetail()
            {
                        $("#id_material_u").val('')
                        $("spesifikasi_u").val('')
            }

   ////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
        $(document).ready(function() {
            $('#id_material').select2();
            // $('#id_material_u').select2();

            $('#example').DataTable( {
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                dom: 'lBfrtip',
            } );

            getSpesifikasi()

        } );

            $(document).on('click', '#btn_add' ,function(){
                // console.log('aw')
                var id_material               = $('#id_material').val();
                var spesifikasi_teknik        = $('#spesifikasi').val();

                            if ( (id_material == '') || (spesifikasi_teknik == '') ) {
                                toastr.warning('Harap lengkapi form Spesifikasi!')
                                } else {
                                    $.post("http://localhost/winnersppmonline/index.php/WEB/spesifikasi", {
                                        id_material             : id_material,
                                        spesifikasi_teknik             : spesifikasi_teknik,
                                        },
                                        function(data, status){
                                        if (status) {
                                            toastr.success('Spesifikasi Baru Dibuat!')
                                            clearTable()
                                            $('#addSpesifikasi').modal('hide')
                                            clearModalAdd()
                                            getSpesifikasi()
                                        }
                                        else {
                                            toastr.error('Spesifikasi Gagal Dibuat!')
                                        }
                                    });
                                }
                            });         

            $(document).on('click', '#btn_edit' ,function(){
                clearModalDetail()
                $('#editSpesifikasi').modal('show')
                var id =  $(this).data("id");
                console.log(id)
                $.ajax({
                    type: "GET",
                    url: 'http://localhost/winnersppmonline/index.php/WEB/spesifikasi/'+id,
                    dataType: 'json',
                    success: function(resp) {
                    console.log(resp)
                        $("#id_u").val(id)
                        $("#id_material_u").val(resp.idmaterial)
                        $("#spesifikasi_u").val(resp.spesifikasi_teknik)

                    },
                    error: function (jqXHR, exception) {
                    }
                });
            });

            $(document).on('click', '#btn_update' ,function(){
                var id                      = $("#id_u").val();
                var id_material             = $("id_material_u").val();
                var spesifikasi_teknik      = $("#spesifikasi_u").val();
                console.log(status)

                if ( (id_material == '') || (spesifikasi == '') )
                {
                    toastr.error('Harap lengkapi data!')
                } else {
                    var data = {
                        id_material : id_material,
                        spesifikasi_teknik : spesifikasi_teknik,
                        }

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/spesifikasi/' + id,
                    type: 'PUT',
                    data : data,
                    success: function(data) {
                        $('#editSpesifikasi').modal('hide')
                        clearModalDetail()
                        clearTable()
                        getSpesifikasi();
                        toastr.success('Sukses Perbarui Spesifikasi!')
                        }
                    });
                }
            });
           
            $(document).on('click', '#btn_delete' ,function(){
                var id                      = $(this).data("id");
                // console.log(status)

                    jQuery.ajax({
                    url: 'http://localhost/winnersppmonline/index.php/WEB/spesifikasi/' + id,
                    type: 'delete',
                    success: function() {
                        clearTable()
                        getSpesifikasi();
                        toastr.success('Spesifikasi sukses dihapus!')
                        }
                    });
            });

            $(document).on('click', '#btn_back', function(){
  window.history.back();
})

      </script>