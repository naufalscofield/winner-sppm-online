<div class="main-panel">
    <!-- Navbar -->

    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
            <li id="btn_back" class="fa fa-chevron-circle-left fa-2x"></li><a class="navbar-brand" href="#pablo">Nomenklatur SDM</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <form class="navbar-form">
                    <?= $this->session->userdata('nm_peg');?>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">person</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" href="<?=base_url();?>index.php/user/logout">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Daftar Nomenklatur SDM</h4>
                            <button data-toggle="modal" id="" data-target="#addNomenklatur" class="btn btn-success btn-round"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Nomenklatur</button>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table">
                                    <thead class=" text-primary">
                                        <tr>
                                            <th width="5">
                                                <center><b>No</b></center>
                                            </th>
                                            <th>
                                                <center><b>Lv1</b></center>
                                            </th>
                                            <th>
                                                <center><b>Lv2</b></center>
                                            </th>
                                            <th>
                                                <center><b>Lv3</b></center>
                                            </th>
                                            <th>
                                                <center><b>Lv4</b></center>
                                            </th>
                                            <th>
                                                <center><b>Lv5</b></center>
                                            </th>
                                            <!-- <th>
                                                <center><b>Parent</b></center>
                                            </th> -->
                                            <th>
                                                <center><b>Kode</b></center>
                                            </th>
                                            <th>
                                                <center><b>Keterangan</b></center>
                                            </th>
                                            <th>
                                                <center><b>Aksi</b></center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addNomenklatur" class="modal lg" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Buat Nomenklatur Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="level">
                                <label>Pilih Level</label><br>
                                <input required type="radio" name="lv" id="rd_lv1" class="">Level 1 <br>
                                <input required type="radio" name="lv" id="rd_lv2" class="">Level 2 <br>
                                <input required type="radio" name="lv" id="rd_lv3" class="">Level 3 <br>
                                <input required type="radio" name="lv" id="rd_lv4" class="">Level 4 <br>
                                <input required type="radio" name="lv" id="rd_lv5" class="">Level 5 <br>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="input_kode_lv1">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="select_kode_lv1">
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="input_kode_lv2">
                                
                            
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="select_kode_lv2">
                                
                               
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="input_kode_lv3">
                                
                          
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="select_kode_lv3">
                                
                               
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="input_kode_lv4">
                                
                            
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="select_kode_lv4">
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="input_kode_lv5">
                                
                            
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="input_keterangan">
                                <label>Keterangan</label>
                                <input required type="text" name="keterangan" id="keterangan" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_add" type="" class="btn btn-success btn-fill btn-wd">
                            Buat Nomenklatur Baru
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="editNomenklatur" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Nomenklatur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="">
                                <label for="">Kode Nomenklatur</label>
                                <input required type="text" disabled name="nomenklatur_u" id="nomenklatur_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="material_edit">
                                <label for="">Keterangan</label>
                                <input required type="hidden" name="id_u" id="id_u" class="form-control">
                                <input required type="text" name="keterangan_u" id="keterangan_u" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="btn_update" type="" class="btn btn-success btn-fill btn-wd">
                            Perbarui Keterangan Nomenklatur
                        </button>

                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src = "<?=base_url();?>assets/ladda/dist/spin.min.js" > </script> 
    <script src = "<?=base_url();?>assets/ladda/dist/ladda.min.js" > </script> 
    <script>
	function myFunction() {
		// Declare variables
		var input, filter, table, tr, td, i, txtValue;
		input = document.getElementById("filterRole");
		filter = input.value.toUpperCase();
		table = document.getElementById("example");
		tr = table.getElementsByTagName("tr");

		// Loop through all table rows, and hide those who don't match the search query
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[6];
			if (td) {
				txtValue = td.textContent || td.innerText;
				if (txtValue.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}
	}

function getNomenklatur() {
	$.get("http://localhost/winnersppmonline/index.php/WEB/nomenklatur", function (data, status) {
		console.log(data, status)
		if (status) {
			$('#table-row').html("")
			let no = 0;
			data.forEach(element => {
				var t = $('#example').DataTable();
				no++;
				if (element.lv1 == null) {
					var lv1 = ''
				} else {
					var lv1 = element.lv1
				}
				if (element.lv2 == null) {
					var lv2 = ''
				} else {
					var lv2 = element.lv2
				}
				if (element.lv3 == null) {
					var lv3 = ''
				} else {
					var lv3 = element.lv3
				}
				if (element.lv4 == null) {
					var lv4 = ''
				} else {
					var lv4 = element.lv4
				}
				if (element.lv5 == null) {
					var lv5 = ''
				} else {
					var lv5 = element.lv5
				}
				t.row.add([
					"<center>" + no + "</center>",
					"<center>" + lv1 + "</center>",
					"<center>" + lv2 + "</center>",
					"<center>" + lv3 + "</center>",
					"<center>" + lv4 + "</center>",
					"<center>" + lv5 + "</center>",
					// "<center>" + element.parent + "</center>",
					"<center>" + lv1 + lv2 + lv3 + lv4 + lv5 + "</center>",
					"<center>" + element.keterangan + "</center>",
					"<center><button type='button' id='btn_edit' data-id=" + element.id + " class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' title='Detail dan Edit Judul'><i class='fa fa-pencil'></i></button><button type='button' id='btn_delete' data-id=" + element.id + " class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Judul'><i class='fa fa-trash'></i></button></center>"
				]).draw(false);

			});
		} else {
			alert('Load Data Failed')
		}
	});
}

function clearTable() {
	var table = $('#example').DataTable();
	table
		.clear()
		.draw();
}

function clearModalAdd() {
	$("#material").val('')
}

function clearModalDetail() {
	$("#material_u").val('')
}

////////////////////////////////////document ready/////////////////////////////////////////////////////////////     
$(document).ready(function () {

	$('#example').DataTable({
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		],
        "columnDefs": [
            { "width": "20%", "targets": 8 }
        ],
		dom: 'lBfrtip',
	});

	getNomenklatur()

});
$(document).on('click', '#rd_lv1', function () {
    var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el1.empty();
    $el11.empty();
    $el2.empty();
    $el22.empty();
    $el3.empty();
    $el33.empty();
    $el4.empty();
    $el44.empty();
    $el5.empty();
    $el1.append($("<label>Input Kode Level 1</label>"))
    $el1.append($("<input type='text' required id='kode_lv1' class='form-control'></input>"))
})

$(document).on('click', '#rd_lv2', function () {
	var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el1.empty();
    $el11.empty();
    $el2.empty();
    $el22.empty();
    $el3.empty();
    $el33.empty();
    $el4.empty();
    $el44.empty();
    $el5.empty();
    $.ajax({
        type: "post",
        url: "http://localhost/winnersppmonline/index.php/WEB/kd_nomenklatur/1",
        dataType: 'text',
        success: function(resp) {
          console.log('resp',resp)
          var json = JSON.parse(resp.replace(',.', ''))
          $el11.append($("<label>Pilih Kode Level 1</label>"))
          $el11.append($("<select required id='opt_kode_lv1' class='form-control'></select>"))
          var $el11s = $('#opt_kode_lv1')
          $el11s.append($("<option></option>")
          .attr("value", '').text('-- Pilih Kode Level 1 --'));
          $.each(json, function(key, value) {
            $el11s.append($("<option></option>")
            .attr("value", value.lv1).text(value.lv1+'-'+value.keterangan));
          });

          $el2.append($("<label>Input Kode Level 2</label>"))
          $el2.append($("<input type='text' required id='kode_lv2' class='form-control'></input>"))
        },
        error: function (jqXHR, exception) {
          console.log(jqXHR, exception)
        }
      });
})

$(document).on('click', '#rd_lv3', function () {
	var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el1.empty();
    $el11.empty();
    $el2.empty();
    $el22.empty();
    $el3.empty();
    $el33.empty();
    $el4.empty();
    $el44.empty();
    $el5.empty();
    $.ajax({
        type: "post",
        url: "http://localhost/winnersppmonline/index.php/WEB/kd_nomenklatur/1",
        dataType: 'text',
        success: function(resp) {
          console.log('resp',resp)
          var json = JSON.parse(resp.replace(',.', ''))
          $el11.append($("<label>Pilih Kode Level 1</label>"))
          $el11.append($("<select required id='opt_kode_lv1' class='form-control'></select>"))
          var $el11s = $('#opt_kode_lv1')
          $el11s.append($("<option></option>")
          .attr("value", '').text('-- Pilih Kode Level 1 --'));
          $.each(json, function(key, value) {
            $el11s.append($("<option></option>")
            .attr("value", value.lv1).text(value.lv1+'-'+value.keterangan));
          });

        },
        error: function (jqXHR, exception) {
          console.log(jqXHR, exception)
        }
      });
          $el3.append($("<label>Input Kode Level 3</label>"))
          $el3.append($("<input type='text' required id='kode_lv3' class='form-control'></input>"))
})

$(document).on('click', '#rd_lv4', function () {
	var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el1.empty();
    $el11.empty();
    $el2.empty();
    $el22.empty();
    $el3.empty();
    $el33.empty();
    $el4.empty();
    $el44.empty();
    $el5.empty();
    $.ajax({
        type: "post",
        url: "http://localhost/winnersppmonline/index.php/WEB/kd_nomenklatur/1",
        dataType: 'text',
        success: function(resp) {
          console.log('resp',resp)
          var json = JSON.parse(resp.replace(',.', ''))
          $el11.append($("<label>Pilih Kode Level 1</label>"))
          $el11.append($("<select required id='opt_kode_lv1' class='form-control'></select>"))
          var $el11s = $('#opt_kode_lv1')
          $el11s.append($("<option></option>")
          .attr("value", '').text('-- Pilih Kode Level 1 --'));
          $.each(json, function(key, value) {
            $el11s.append($("<option></option>")
            .attr("value", value.lv1).text(value.lv1+'-'+value.keterangan));
          });

        },
        error: function (jqXHR, exception) {
          console.log(jqXHR, exception)
        }
      });
          $el4.append($("<label>Input Kode Level 4</label>"))
          $el4.append($("<input type='text' required id='kode_lv4' class='form-control'></input>"))
})

$(document).on('click', '#rd_lv5', function () {
	var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el1.empty();
    $el11.empty();
    $el2.empty();
    $el22.empty();
    $el3.empty();
    $el33.empty();
    $el4.empty();
    $el44.empty();
    $el5.empty();
    $.ajax({
        type: "post",
        url: "http://localhost/winnersppmonline/index.php/WEB/kd_nomenklatur/1",
        dataType: 'text',
        success: function(resp) {
          console.log('resp',resp)
          var json = JSON.parse(resp.replace(',.', ''))
          $el11.append($("<label>Pilih Kode Level 1</label>"))
          $el11.append($("<select required id='opt_kode_lv1' class='form-control'></select>"))
          var $el11s = $('#opt_kode_lv1')
          $el11s.append($("<option></option>")
          .attr("value", '').text('-- Pilih Kode Level 1 --'));
          $.each(json, function(key, value) {
            $el11s.append($("<option></option>")
            .attr("value", value.lv1).text(value.lv1+'-'+value.keterangan));
          });

        },
        error: function (jqXHR, exception) {
          console.log(jqXHR, exception)
        }
      });
          $el5.append($("<label>Input Kode Level 5</label>"))
          $el5.append($("<input type='text' required id='kode_lv5' class='form-control'></input>"))
})

$(document).on('change', '#opt_kode_lv1', function () {
    if( ($('#rd_lv3').prop('checked') == true) || ($('#rd_lv4').prop('checked') == true) || ($('#rd_lv5').prop('checked') == true) ) {
    var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el22.empty();
    $.ajax({
        type: "post",
        data : {
            lv1 : $('#opt_kode_lv1').val()
        },
        url: "http://localhost/winnersppmonline/index.php/WEB/kd_nomenklatur/2",
        dataType: 'text',
        success: function(resp) {
          console.log('resp',resp)
          var json = JSON.parse(resp.replace(',.', ''))
          $el22.append($("<label>Pilih Kode Level 2</label>"))
          $el22.append($("<select required id='opt_kode_lv2' class='form-control'></select>"))
          var $el22s = $('#opt_kode_lv2')
          $el22s.append($("<option></option>")
          .attr("value", '').text('-- Pilih Kode Level 2 --'));
          $.each(json, function(key, value) {
            $el22s.append($("<option></option>")
            .attr("value", value.lv2).text(value.lv2+'-'+value.keterangan));
          });

        },
        error: function (jqXHR, exception) {
          console.log(jqXHR, exception)
        }
      });
    }
})

$(document).on('change', '#opt_kode_lv2', function () {
    if( ($('#rd_lv4').prop('checked') == true ) || ($('#rd_lv5').prop('checked') == true ) )   {
    var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el33.empty();
    $.ajax({
        type: "post",
        data : {
            lv1 : $('#opt_kode_lv1').val()
        },
        url: "http://localhost/winnersppmonline/index.php/WEB/kd_nomenklatur/3",
        dataType: 'text',
        success: function(resp) {
          console.log('resp',resp)
          var json = JSON.parse(resp.replace(',.', ''))
          $el33.append($("<label>Pilih Kode Level 3</label>"))
          $el33.append($("<select required id='opt_kode_lv3' class='form-control'></select>"))
          var $el33s = $('#opt_kode_lv3')
          $el33s.append($("<option></option>")
          .attr("value", '').text('-- Pilih Kode Level 3 --'));
          $.each(json, function(key, value) {
            $el33s.append($("<option></option>")
            .attr("value", value.lv3).text(value.lv3+'-'+value.keterangan));
          });

        },
        error: function (jqXHR, exception) {
          console.log(jqXHR, exception)
        }
      });
    }
})

$(document).on('change', '#opt_kode_lv3', function () {
    if($('#rd_lv5').prop('checked') == true)   {
    var $el1 = $("#input_kode_lv1");
    var $el11 = $("#select_kode_lv1");
    var $el2 = $("#input_kode_lv2");
    var $el22 = $("#select_kode_lv2");
    var $el3 = $("#input_kode_lv3");
    var $el33 = $("#select_kode_lv3");
    var $el4 = $("#input_kode_lv4");
    var $el44 = $("#select_kode_lv4");
    var $el5 = $("#input_kode_lv5");
    $el44.empty();
    $.ajax({
        type: "post",
        data : {
            lv1 : $('#opt_kode_lv1').val()
        },
        url: "http://localhost/winnersppmonline/index.php/WEB/kd_nomenklatur/4",
        dataType: 'text',
        success: function(resp) {
          console.log('resp',resp)
          var json = JSON.parse(resp.replace(',.', ''))
          $el44.append($("<label>Pilih Kode Level 4</label>"))
          $el44.append($("<select required id='opt_kode_lv4' class='form-control'></select>"))
          var $el44s = $('#opt_kode_lv4')
          $el44s.append($("<option></option>")
          .attr("value", '').text('-- Pilih Kode Level 4 --'));
          $.each(json, function(key, value) {
            $el44s.append($("<option></option>")
            .attr("value", value.lv4).text(value.lv4+'-'+value.keterangan));
          });

        },
        error: function (jqXHR, exception) {
          console.log(jqXHR, exception)
        }
      });
    }
})



$(document).on('click', '#btn_add', function () {
			if ($('#rd_lv1').prop('checked') == true) {
				var lv1 = $('#kode_lv1').val();
				var keterangan = $('#keterangan').val();
				var level = 1;

				if ((lv1 == '') || (keterangan == '')) {
					console.log(lv1, keterangan)
					toastr.warning('Harap lengkapi form !')
				} else {
					$.post("http://localhost/winnersppmonline/index.php/WEB/nomenklatur", {
							lv1: lv1,
							keterangan: keterangan,
							level: level,
						},
						function (data, status) {
							if (data.status == true) {
								toastr.success(data.message)
								clearTable()
								$('#addNomenklatur').modal('hide')
								clearModalAdd()
								getNomenklatur()
							} else {
								toastr.error(data.message)
							}
						});
				}
			} else if ($('#rd_lv2').prop('checked') == true) {
				var lv1 = $('#opt_kode_lv1').val();
				var lv2 = $('#kode_lv2').val();
				var parent = $('#opt_kode_lv1').val();
				var keterangan = $('#keterangan').val();
				var level = 2;

				if ((lv1 == '') || (lv2 == '') || (keterangan == '')) {
					console.log(lv1, keterangan)
					toastr.warning('Harap lengkapi form !')
				} else {
					$.post("http://localhost/winnersppmonline/index.php/WEB/nomenklatur", {
							lv1: lv1,
							lv2: lv2,
							parent: parent,
							keterangan: keterangan,
							level: level,
						},
						function (data, status) {
							if (data.status == true) {
								toastr.success(data.message)
								clearTable()
								$('#addNomenklatur').modal('hide')
								clearModalAdd()
								getNomenklatur()
							} else {
								toastr.error(data.message)
							}
						});
				}
            } else if ($('#rd_lv3').prop('checked') == true) {
				var lv1 = $('#opt_kode_lv1').val();
				var lv2 = $('#opt_kode_lv2').val();
				var lv3 = $('#kode_lv3').val();
				var parent = $('#opt_kode_lv2').val();
				var keterangan = $('#keterangan').val();
				var level = 3;

				if ((lv1 == '') || (lv2 == '') || (lv3 == '') || (keterangan == '')) {
					console.log(lv1, keterangan)
					toastr.warning('Harap lengkapi form !')
				} else {
					$.post("http://localhost/winnersppmonline/index.php/WEB/nomenklatur", {
							lv1: lv1,
							lv2: lv2,
							lv3: lv3,
							parent: parent,
							keterangan: keterangan,
							level: level,
						},
						function (data, status) {
							if (data.status == true) {
								toastr.success(data.message)
								clearTable()
								$('#addNomenklatur').modal('hide')
								clearModalAdd()
								getNomenklatur()
							} else {
								toastr.error(data.message)
							}
						});
				}
            } else if ($('#rd_lv4').prop('checked') == true) {
				var lv1 = $('#opt_kode_lv1').val();
				var lv2 = $('#opt_kode_lv2').val();
				var lv3 = $('#opt_kode_lv3').val();
				var lv4 = $('#kode_lv4').val();
				var parent = $('#opt_kode_lv3').val();
				var keterangan = $('#keterangan').val();
				var level = 4;

				if ((lv1 == '') || (lv2 == '') || (lv3 == '') || (lv4 == '') || (keterangan == '')) {
					console.log(lv1, keterangan)
					toastr.warning('Harap lengkapi form !')
				} else {
					$.post("http://localhost/winnersppmonline/index.php/WEB/nomenklatur", {
							lv1: lv1,
							lv2: lv2,
							lv3: lv3,
							lv4: lv4,
							parent: parent,
							keterangan: keterangan,
							level: level,
						},
						function (data, status) {
							if (data.status == true) {
								toastr.success(data.message)
								clearTable()
								$('#addNomenklatur').modal('hide')
								clearModalAdd()
								getNomenklatur()
							} else {
								toastr.error(data.message)
							}
						});
				}
            } else if ($('#rd_lv5').prop('checked') == true) {
				var lv1 = $('#opt_kode_lv1').val();
				var lv2 = $('#opt_kode_lv2').val();
				var lv3 = $('#opt_kode_lv3').val();
				var lv4 = $('#opt_kode_lv4').val();
				var lv5 = $('#kode_lv5').val();
				var parent = $('#opt_kode_lv4').val();
				var keterangan = $('#keterangan').val();
				var level = 5;

				if ((lv1 == '') || (lv2 == '') || (lv3 == '') || (lv4 == '') || (lv5 == '') || (keterangan == '')) {
					console.log(lv1, keterangan)
					toastr.warning('Harap lengkapi form !')
				} else {
					$.post("http://localhost/winnersppmonline/index.php/WEB/nomenklatur", {
							lv1: lv1,
							lv2: lv2,
							lv3: lv3,
							lv4: lv4,
							lv5: lv5,
							parent: parent,
							keterangan: keterangan,
							level: level,
						},
						function (data, status) {
							if (data.status == true) {
								toastr.success(data.message)
								clearTable()
								$('#addNomenklatur').modal('hide')
								clearModalAdd()
								getNomenklatur()
							} else {
								toastr.error(data.message)
							}
						});
				}
            }

			});

		$(document).on('click', '#btn_edit', function () {
			clearModalDetail()
			$('#editNomenklatur').modal('show')
			var id = $(this).data("id");
			console.log(id)
			$.ajax({
				type: "GET",
				url: 'http://localhost/winnersppmonline/index.php/WEB/nomenklatur/' + id,
				dataType: 'json',
				success: function (resp) {
                if (resp.lv1 == null) {
					var lv1 = ''
				} else {
					var lv1 = resp.lv1
				}
				if (resp.lv2 == null) {
					var lv2 = ''
				} else {
					var lv2 = resp.lv2
				}
				if (resp.lv3 == null) {
					var lv3 = ''
				} else {
					var lv3 = resp.lv3
				}
				if (resp.lv4 == null) {
					var lv4 = ''
				} else {
					var lv4 = resp.lv4
				}
				if (resp.lv5 == null) {
					var lv5 = ''
				} else {
					var lv5 = resp.lv5
				}
					$("#nomenklatur_u").val(lv1+lv2+lv3+lv4+lv5)
					$("#keterangan_u").val(resp.keterangan)
					$("#id_u").val(id)

				},
				error: function (jqXHR, exception) {}
			});
		});

		$(document).on('click', '#btn_update', function () {
			var id = $("#id_u").val();
			var keterangan = $("#keterangan_u").val();

			if (keterangan == '') {
				toastr.error('Harap lengkapi data!')
			} else {
				var data = {
					keterangan: keterangan,
				}

				jQuery.ajax({
					url: 'http://localhost/winnersppmonline/index.php/WEB/nomenklatur/' + id,
					type: 'PUT',
					data: data,
					success: function (data) {
						if (data.status == true) {
							$('#editNomenklatur').modal('hide')
							clearModalDetail()
							clearTable()
							getNomenklatur();
							toastr.success('Sukses Perbarui Nomenklatur!')

						} else {
							toastr.error(data.message)
						}
					}
				});
			}
		});

		$(document).on('click', '#btn_delete', function () {
			var id = $(this).data("id");
			// console.log(status)

			jQuery.ajax({
				url: 'http://localhost/winnersppmonline/index.php/WEB/nomenklatur/' + id,
				type: 'delete',
				success: function () {
					clearTable()
					getNomenklatur();
					toastr.success('Nomenklatur sukses dihapus!')
				}
			});
		});

        $(document).on('click', '#btn_back', function(){
  window.history.back();
})

		</script>