<?php
$uri = $this->uri->segment(2);
?>
<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?=base_url();?>assets/main_bootstrap/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          <img src="<?=base_url();?>assets/main_bootstrap/img/logo-wika.png" style="height:85px;width:220px" alt="">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">

        <!-- <?php if ($uri == ''){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User">
              <i class="material-icons">dashboard</i>
              <p>Dashboard & Reports</p>
            </a>
          </li> -->

          <?php if ($uri == 'getProfile'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>  
            <a class="nav-link" href="<?=base_url();?>index.php/User/getProfile">
              <i class="material-icons">person</i>
              <p>Profil</p>
            </a>
          </li>
          
          <?php if ($this->session->userdata('admin') ==  'yes') { ?>
          <?php if ($uri == 'getPegawai'){ ?>
              <li class="nav-item active">
          <?php } else { ?> 
              <li class="nav-item">
          <?php } ?>    
            <a class="nav-link" href="<?=base_url();?>index.php/User/getPegawai">
              <i class="fa fa-users" aria-hidden="true"></i>
              <p>Pegawai</p>
            </a>
          </li>
          <?php } ?>

          <?php if  ($uri == 'getBiro'){ ?>
                <li class="nav-item active">
            <?php } else { ?> 
                <li class="nav-item">
            <?php } ?>    
              <a class="nav-link" href="<?=base_url();?>index.php/User/getBiro">
              <i class="material-icons">library_books</i>
                <p>Biro</p>
              </a>
            </li>
         
          <?php if ($this->session->userdata('admin') ==  'no') { ?>
            <?php if (($uri == 'getSPPM') || ($uri == 'getSPPMMat')){ ?>
                <li class="nav-item active">
            <?php } else { ?> 
                <li class="nav-item">
            <?php } ?>    
              <a class="nav-link" href="<?=base_url();?>index.php/User/getSPPM">
              <i class="fa fa-file-text"></i>
                <p>SPPM Produk</p>
              </a>
            </li>
            <?php } ?>    
            
            <?php if ($this->session->userdata('admin') ==  'no') { ?>
            <?php if (( $uri == 'getSPPM2') || ($uri == 'getSPPMMat2')){ ?>
                <li class="nav-item active">
            <?php } else { ?> 
                <li class="nav-item">
            <?php } ?>    
              <a class="nav-link" href="<?=base_url();?>index.php/User/getSPPM2">
              <i class="fa fa-file"></i>
                <p>SPPM Material</p>
              </a>
            </li>
            <?php } ?>    
            
            <?php if ( ($uri == 'getJudul') || ($uri == 'getTMaterial') ){ ?>
                <li class="nav-item active">
            <?php } else { ?> 
                <li class="nav-item">
            <?php } ?>    
              <a class="nav-link" href="<?=base_url();?>index.php/User/getJudul">
              <i class="fa fa-quote-left"></i>
                <p>Judul</p>
              </a>
            </li>
            
            <?php if  ( ($uri == 'getMaterial') || ($uri == 'getTMaterial') ){ ?>
                <li class="nav-item active">
            <?php } else { ?> 
                <li class="nav-item">
            <?php } ?>    
              <a class="nav-link" href="<?=base_url();?>index.php/User/getMaterial">
              <i class="fa fa-cube"></i>
                <p>Material</p>
              </a>
            </li>

            <?php if  ($uri == 'getNomenKlaturSDM'){ ?>
                <li class="nav-item active">
            <?php } else { ?> 
                <li class="nav-item">
            <?php } ?>    
              <a class="nav-link" href="<?=base_url();?>index.php/User/getNomenKlaturSDM">
              <i class="fa fa-codepen"></i>
                <p>Nomenklatur SDM</p>
              </a>
            </li>

            <?php if  ($uri == 'getSpesifikasi'){ ?>
                <li class="nav-item active">
            <?php } else { ?> 
                <li class="nav-item">
            <?php } ?>    
              <a class="nav-link" href="<?=base_url();?>index.php/User/getSpesifikasi">
              <i class="fa fa-cogs"></i>
                <p>Spesifikasi Teknik</p>
              </a>
            </li>

        </ul>
      </div>
    </div>