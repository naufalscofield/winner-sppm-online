<?php

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status_login')){
			redirect('index.php/user/getProfile');
		}
	}
	public function index()
	{
		$this->load->view('login');
	}
}
