<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
    
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		// $this->load->model('model');
		if ($this->session->userdata('status_login') != 'login'){
			redirect('index.php/login');
		}	
	}

	public function index()
	{
        // print_r($this->session->userdata('admin')); die;
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/dashboard');
		$this->load->view('components/footer');
	}
	
	public function getPegawai()
	{
        $data['biro'] = $this->db->get('biro')->result_array();
        $data['biro_'] = $this->db->get('biro')->result_array();
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/pegawai',$data);
		$this->load->view('components/footer');
	}
	
	public function getSPPM()
	{
		$role = $this->session->userdata('role');
		$data['judul'] = $this->main_model->getJudul();
		$data['manager'] = $this->main_model->getManager();
		$data['koordinator'] = $this->main_model->getKoordinator();

		$this->load->view('components/header');
		$this->load->view('components/sidebar');

		if ($role == 'pegawai') {
			$this->load->view('user/sppm',$data);
		} else if ($role == 'koordinator') {
			$this->load->view('user/sppm-k',$data);
		} else if ($role == 'manager') {
			$this->load->view('user/sppm-m',$data);
		} else {
			$this->load->view('user/sppm-p',$data);
		}
		
		$this->load->view('components/footer');
	}
	
	public function getSPPM2()
	{	
		$role = $this->session->userdata('role');
		$data['manager'] = $this->main_model->getManager();
		$data['koordinator'] = $this->main_model->getKoordinator();

		$this->load->view('components/header');
		$this->load->view('components/sidebar');

		if ($role == 'pegawai') {
			$this->load->view('user/sppm2',$data);
		} else if ($role == 'koordinator') {
			$this->load->view('user/sppm2-k',$data);
		} else if ($role == 'manager') {
			$this->load->view('user/sppm2-m',$data);
		} else {
			$this->load->view('user/sppm2-p',$data);
		}
		
		$this->load->view('components/footer');
	}
	
	public function getJudul()
	{
		$data['manager'] = $this->main_model->getManager();
		$data['koordinator'] = $this->main_model->getKoordinator();

		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/judul',$data);
		$this->load->view('components/footer');
	}
	
	public function getMaterial()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/material');
		$this->load->view('components/footer');
	}
	
	public function getTMaterial($id)
	{
		$data['judul'] = $this->main_model->getJudul($id);
		$data['material'] = $this->main_model->getMaterial();
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/tmaterial',$data);
		$this->load->view('components/footer');
	}
	
	public function getSpesifikasi()
	{
		$data['material'] = $this->main_model->getMaterial();
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/spesifikasi',$data);
		$this->load->view('components/footer');
	}
	
	public function getSPPMMat($id_sppm)
	{
		$data['sppm'] = $this->main_model->getSPPM($id_sppm);
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/SPPMmat',$data);
		$this->load->view('components/footer');
	}
	
	public function getSPPMMat2($id_sppm)
	{
		$data['sppm'] = $this->main_model->getSPPM2($id_sppm);
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/SPPMmat2',$data);
		$this->load->view('components/footer');
	}
	
	public function reportSPPM2($id_sppm)
	{
			$sppm = $this->main_model->getSPPM2($id_sppm);
			$sppm2 = $this->main_model->getSPPM2Report($id_sppm);
			// print_r($mat); die;
			$sppmmat = $this->main_model->getSPPMMAT2($id_sppm);
			$no_sppm = $sppm->no_sppm;
			$no_sppm = str_replace('/', '-', $no_sppm);

			$spreadsheet = new Spreadsheet();
			$spreadsheet->getDefaultStyle()->getFont()->setSize(12);

			//merge
			// $spreadsheet->getActiveSheet()->mergeCells('M2:O2');
			// $spreadsheet->getActiveSheet()->mergeCells('M3:O3');
			// $spreadsheet->getActiveSheet()->mergeCells('M4:O4');
			// $spreadsheet->getActiveSheet()->mergeCells('M5:O5');
			$spreadsheet->getActiveSheet()->mergeCells('F6:L6');
			$spreadsheet->getActiveSheet()->mergeCells('F7:L7');
			$spreadsheet->getActiveSheet()->mergeCells('F9:F10');
			$spreadsheet->getActiveSheet()->mergeCells('G9:G10');
			$spreadsheet->getActiveSheet()->mergeCells('H9:I9');
			$spreadsheet->getActiveSheet()->mergeCells('J9:J10');
			$spreadsheet->getActiveSheet()->mergeCells('K9:K10');
			$spreadsheet->getActiveSheet()->mergeCells('L9:L10');

			//style
			$boldTembusan = [
				'font' => [
					'bold' => true,
				],
				'alignment' => [
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
				]
			];
			$isiTembusan = [
				'alignment' => [
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
				],
			];
			$borderTembusan = [
				'borders' => [
					'outline' => [
						'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
						'color' => ['argb' => '000'],
					],
				],
			];
			$SPP = [
				'font' => [
					'bold' => true,
					'underline' => true,
					'size' => 14,
					'name' => 'Arial Black'
				],
				'alignment' => [
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				]
			];
			$NOSPPM = [
				'font' => [
					'size' => 13,
				],
				'alignment' => [
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				]
			];
			$namakolom = [
				'font' => [
					'size' => 12,
					'bold' => true,
				],
				'alignment' => [
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				]
			];
			$isikolom = [
				'font' => [
					'size' => 12,
				],
				'alignment' => [
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				]
			];
			$namausers = [
				'font' => [
					'size' => 12,
					'underline' => true,
				],
				'alignment' => [
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				]
			];

			$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(8);
			$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(40);
			$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(40);
			$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(30);
			$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(30);
			$spreadsheet->getActiveSheet()->getStyle('M2')->applyFromArray($boldTembusan);
			$spreadsheet->getActiveSheet()->getStyle('M3')->applyFromArray($isiTembusan);
			$spreadsheet->getActiveSheet()->getStyle('M4')->applyFromArray($isiTembusan);
			$spreadsheet->getActiveSheet()->getStyle('M5')->applyFromArray($isiTembusan);
			$spreadsheet->getActiveSheet()->getStyle('F6')->applyFromArray($SPP);
			$spreadsheet->getActiveSheet()->getStyle('F7')->applyFromArray($NOSPPM);
			$spreadsheet->getActiveSheet()->getStyle('F9')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('G9')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('H9')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('H10')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('I10')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('J9')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('K9')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('L9')->applyFromArray($namakolom);
			$spreadsheet->getActiveSheet()->getStyle('L2:L5')->applyFromArray($borderTembusan);
			$spreadsheet->getActiveSheet()->getStyle('F9:F10')->applyFromArray($borderTembusan);
			$spreadsheet->getActiveSheet()->getStyle('G9:G10')->applyFromArray($borderTembusan);
			$spreadsheet->getActiveSheet()->getStyle('H9:I9')->applyFromArray($borderTembusan);
			$spreadsheet->getActiveSheet()->getStyle('H10:I10')->applyFromArray($borderTembusan);
			$spreadsheet->getActiveSheet()->getStyle('J9:J10')->applyFromArray($borderTembusan);
			$spreadsheet->getActiveSheet()->getStyle('K9:K10')->applyFromArray($borderTembusan);
			$spreadsheet->getActiveSheet()->getStyle('L9:L10')->applyFromArray($borderTembusan);

			//drawing
			$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
			$drawing->setName('Logo');
			$drawing->setDescription('Logo');
			$drawing->setPath('./assets/main_bootstrap/img/winner.png');
			// $drawing->setHeight(10);
			$drawing->setWidth(216);
			$drawing->setCoordinates('F2');
			$drawing->setWorksheet($spreadsheet->getActiveSheet());

            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('L2', 'Tembusan :');
            $sheet->setCellValue('L3', '1. Arsip PEP');
            $sheet->setCellValue('L4', '2. Arsip Pemohon');
            $sheet->setCellValue('L5', '3. Gudang');
            $sheet->setCellValue('F6', 'SURAT PERMINTAAN PENGADAAN (SPP)');
            $sheet->setCellValue('F7', "$no_sppm");
            $sheet->setCellValue('F9', 'No.');
            $sheet->setCellValue('G9', 'Nama Material // Merek');
            $sheet->setCellValue('H9', 'Kuantitas');
            $sheet->setCellValue('H10', 'Volume');
            $sheet->setCellValue('I10', 'Satuan');
            $sheet->setCellValue('J9', 'Spesifikasi Teknik');
            $sheet->setCellValue('K9', 'Harga');
			$sheet->setCellValue('L9', 'Keterangan Untuk');
			
			//foreach
			$hurufNo = 'F';
			$angkaNo = 10;
			$hurufNama = 'G';
			$angkaNama = 10;
			$hurufVolume = 'H';
			$angkaVolume = 10;
			$hurufSatuan = 'I';
			$angkaSatuan = 10;
			$hurufSpek = 'J';
			$angkaSpek = 10;
			$hurufHarga = 'K';
			$angkaHarga = 10;
			$hurufKet = 'L';
			$angkaKet = 10;

			$no=0;

			foreach($sppmmat as $mat){

				$angkaNo++;
				$angkaNama++;
				$angkaVolume++;
				$angkaSatuan++;
				$angkaSpek++;
				$angkaHarga++;
				$angkaKet++;
				$no++;

				$sheet->setCellValue("$hurufNo$angkaNo", "$no");
				$spreadsheet->getActiveSheet()->getStyle("$hurufNo$angkaNo")->applyFromArray($isikolom);
				$spreadsheet->getActiveSheet()->getStyle("$hurufNo$angkaNo")->applyFromArray($borderTembusan);
				
				$sheet->setCellValue("$hurufNama$angkaNama", $mat['material_mat'].' / '.$mat['merek']);
				$spreadsheet->getActiveSheet()->getStyle("$hurufNama$angkaNama")->applyFromArray($isikolom);
				$spreadsheet->getActiveSheet()->getStyle("$hurufNama$angkaNama")->applyFromArray($borderTembusan);
				
				$sheet->setCellValue("$hurufVolume$angkaVolume", $mat['qty']);
				$spreadsheet->getActiveSheet()->getStyle("$hurufVolume$angkaVolume")->applyFromArray($isikolom);
				$spreadsheet->getActiveSheet()->getStyle("$hurufVolume$angkaVolume")->applyFromArray($borderTembusan);
				
				$sheet->setCellValue("$hurufSatuan$angkaSatuan", $mat['satuan']);
				$spreadsheet->getActiveSheet()->getStyle("$hurufSatuan$angkaSatuan")->applyFromArray($isikolom);
				$spreadsheet->getActiveSheet()->getStyle("$hurufSatuan$angkaSatuan")->applyFromArray($borderTembusan);
				
				$sheet->setCellValue("$hurufSpek$angkaSpek", $mat['spesifikasi_teknik_mat']);
				$spreadsheet->getActiveSheet()->getStyle("$hurufSpek$angkaSpek")->applyFromArray($isikolom);
				$spreadsheet->getActiveSheet()->getStyle("$hurufSpek$angkaSpek")->applyFromArray($borderTembusan);
				
				$sheet->setCellValue("$hurufHarga$angkaHarga", $mat['harga']);
				$spreadsheet->getActiveSheet()->getStyle("$hurufHarga$angkaHarga")->applyFromArray($isikolom);
				$spreadsheet->getActiveSheet()->getStyle("$hurufHarga$angkaHarga")->applyFromArray($borderTembusan);
				
				$sheet->setCellValue("$hurufKet$angkaKet", "-");
				$spreadsheet->getActiveSheet()->getStyle("$hurufKet$angkaKet")->applyFromArray($isikolom);
				$spreadsheet->getActiveSheet()->getStyle("$hurufKet$angkaKet")->applyFromArray($borderTembusan);

			}

			$angkaRa = $angkaHarga + 1;
			$sheet->setCellValue("$hurufHarga$angkaRa", "Ra. Kedatangan tgl.");
			$spreadsheet->getActiveSheet()->getStyle("$hurufHarga$angkaRa")->applyFromArray($isikolom);
			$spreadsheet->getActiveSheet()->getStyle("$hurufHarga$angkaRa")->applyFromArray($borderTembusan);
			
			$angkaTargetDatang = $angkaKet + 1;
			$targetDatang = $sppm->target_delivery;
			$targetDatang = date('d F, Y', strtotime($targetDatang));
			$sheet->setCellValue("$hurufKet$angkaTargetDatang", "$targetDatang");
			$spreadsheet->getActiveSheet()->getStyle("$hurufKet$angkaTargetDatang")->applyFromArray($isikolom);
			$spreadsheet->getActiveSheet()->getStyle("$hurufKet$angkaTargetDatang")->applyFromArray($borderTembusan);

			$angkaTanggal = $angkaRa + 2;
			$angkaTanggal2 = $angkaTargetDatang + 2;
			$date = date('Y-m-d');
			$date = date('d F, Y', strtotime($date));
			$spreadsheet->getActiveSheet()->mergeCells("$hurufHarga$angkaTanggal:$hurufKet$angkaTanggal2");
			$sheet->setCellValue("$hurufHarga$angkaTanggal", "Cibinong, $date");
			$spreadsheet->getActiveSheet()->getStyle("$hurufHarga$angkaTanggal")->applyFromArray($isikolom);

			$angkaSudahTTD = $angkaNo + 3;
			$angkaSudahTTD2 = $angkaNama + 3;
			$spreadsheet->getActiveSheet()->mergeCells("$hurufNo$angkaSudahTTD:$hurufNama$angkaSudahTTD2");
			$sheet->setCellValue("$hurufNo$angkaSudahTTD", "Sudah Ter TTD Online Oleh:");
			$spreadsheet->getActiveSheet()->getStyle("$hurufNo$angkaSudahTTD")->applyFromArray($isikolom);

			$angkaPembuat = $angkaSudahTTD2 + 2;
			$sheet->setCellValue("$hurufNama$angkaPembuat", "Pembuat SPPM:");
			$spreadsheet->getActiveSheet()->getStyle("$hurufNama$angkaPembuat")->applyFromArray($namakolom);
			$angkaPembuat2 = $angkaSudahTTD2 + 3;
			$sheet->setCellValue("$hurufNama$angkaPembuat2", "$sppm2->nama_peminta");
			$spreadsheet->getActiveSheet()->getStyle("$hurufNama$angkaPembuat2")->applyFromArray($namausers);
			
			$angkaKoordinator = $angkaSpek + 5;
			$sheet->setCellValue("$hurufSpek$angkaKoordinator", "Koordinator:");
			$spreadsheet->getActiveSheet()->getStyle("$hurufSpek$angkaKoordinator")->applyFromArray($namakolom);
			$angkaKoordinator2 = $angkaSpek + 6;
			$sheet->setCellValue("$hurufSpek$angkaKoordinator2", "$sppm2->nama_koordinator");
			$spreadsheet->getActiveSheet()->getStyle("$hurufSpek$angkaKoordinator2")->applyFromArray($namausers);

			$angkaManager = $angkaHarga + 5;
			$sheet->setCellValue("$hurufHarga$angkaManager", "Manager:");
			$spreadsheet->getActiveSheet()->getStyle("$hurufHarga$angkaManager")->applyFromArray($namakolom);
			$angkaManager2 = $angkaHarga + 6;
			$sheet->setCellValue("$hurufHarga$angkaManager2", "$sppm2->nama_manager");
			$spreadsheet->getActiveSheet()->getStyle("$hurufHarga$angkaManager2")->applyFromArray($namausers);
			
			$angkaPengadaan = $angkaKet + 5;
			$sheet->setCellValue("$hurufKet$angkaPengadaan", "Pengadaan:");
			$spreadsheet->getActiveSheet()->getStyle("$hurufKet$angkaPengadaan")->applyFromArray($namakolom);
			$angkaPengadaan2 = $angkaKet + 6;
			$sheet->setCellValue("$hurufKet$angkaPengadaan2", "$sppm2->nama_staf_pengadaan");
			$spreadsheet->getActiveSheet()->getStyle("$hurufKet$angkaPengadaan2")->applyFromArray($namausers);

			$desktop = getenv("HOMEDRIVE").getenv("HOMEPATH")."\Desktop";

			$writer = new Xlsx($spreadsheet);
			$writer->save("$desktop/$no_sppm.xlsx");
			
			echo"<script>alert('File disimpan di direktori $desktop/$no_sppm.xlsx'); window.location = '../getSPPM2'</script>";
	
	}
	
	public function getBiro()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/biro');
		$this->load->view('components/footer');
	}
	
	public function getNomenklaturSDM()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/nomenklatur');
		$this->load->view('components/footer');
	}
	
	public function getSettings()
	{
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/settings');
		$this->load->view('components/footer');
	}
	
	public function getProfile()
	{
		$data['biro'] = $this->db->get('biro')->result_array();
		$data['user'] = array (
			array(

			'id' 				=> $this->session->userdata('id'),
			'nip' 				=> $this->session->userdata('nip'),
			'nm_peg' 			=> $this->session->userdata('nm_peg'),
			'password' 			=> $this->session->userdata('password'),
			'jns_kelamin_peg' 	=> $this->session->userdata('jns_kelamin_peg'),
			'alamat' 			=> $this->session->userdata('alamat'),
			'kodepos' 			=> $this->session->userdata('kodepos'),
			'telepon' 			=> $this->session->userdata('telepon'),
			'handphone'			=> $this->session->userdata('handphone'),
			'email' 			=> $this->session->userdata('email'),
			'kd_jabatan' 		=> $this->session->userdata('kd_jabatan'),
			'nm_jabatan' 		=> $this->session->userdata('nm_jabatan'),
			'kd_unit_org' 		=> $this->session->userdata('kd_unit_org'),
			'nm_unit_org' 		=> $this->session->userdata('nm_unit_org'),
			'id_biro' 			=> $this->session->userdata('id_biro'),
			'status_login' 		=> $this->session->userdata('status_login'),
			'pw' 				=> $this->session->userdata('pw')
			)
		);
		
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view('user/profile',$data);
		$this->load->view('components/footer');
	}

	public function logout()
    {
        $this->session->sess_destroy();
        redirect('index.php/login');
    }

}
