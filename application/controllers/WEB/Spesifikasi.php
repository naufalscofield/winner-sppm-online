<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Spesifikasi extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0, $id_material = 0)
	{
        if(!empty($id)){
            $this->db->join('material a', 'a.id = c.id_material');
            $this->db->select('*,a.id as idmaterial, c.id as idspesifikasi');
            $data = $this->db->get_where("spesifikasi_teknik c", ['c.id' => $id])->row_array();
        }else if(!empty($id_material)){
            $data = $this->db->get_where("spesifikasi_teknik c", ['c.id_material' => $id_material])->result_array();
        } else {
            $this->db->join('material a', 'a.id = c.id_material');
            $this->db->select('*,a.id as idmaterial, c.id as idspesifikasi');
            $data = $this->db->get("spesifikasi_teknik c")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $data = array(
            'id_material' => $this->input->post('id_material'),
            'spesifikasi_teknik' => $this->input->post('spesifikasi_teknik'),
        );
        $this->db->insert('spesifikasi_teknik',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('spesifikasi_teknik', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('spesifikasi_teknik', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}