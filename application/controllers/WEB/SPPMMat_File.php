<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SPPMMat_File extends CI_Controller {
    public function index()
	{
		$id_sppmmat = $this->input->post('id_material_u_2');
		$uri = $this->input->post('uri');
		// print_r($id_sppmmat); die;
		$config['upload_path'] = './assets/file_spesifikasi_teknik_material/';
		$config['allowed_types'] = 'gif|jpg|png|pdf|xlsx';
		$config['max_size'] = '10000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file_u')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->main_model->insertFile($name, $id_sppmmat);
			redirect('index.php/user/getSPPMMat/'.$uri);
		}
	}
}