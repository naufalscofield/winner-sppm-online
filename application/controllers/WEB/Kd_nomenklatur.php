<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Kd_nomenklatur extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_post($level = 0)
	{
        
        if($level == 1){
            $this->db->distinct();
            $this->db->select('lv1, keterangan');
            $data = $this->db->get_where("nomenklatur", ['level' => 1])->result_array();
            // $data = json_encode($data);
        } else if ($level == 2){
            $lv1 = $this->input->post('lv1');
            $this->db->distinct();
            $this->db->select('lv2, keterangan');
            $data = $this->db->get_where("nomenklatur", ['level' => 2, 'lv1' => $lv1])->result_array();
            // $data = json_encode($data);
        } else if ($level == 3){
            $lv1 = $this->input->post('lv1');
            $this->db->distinct();
            $this->db->select('lv3, keterangan');
            $data = $this->db->get_where("nomenklatur", ['level' => 3, 'lv1' => $lv1])->result_array();
            // $data = json_encode($data);
        } else if ($level == 4){
            $lv1 = $this->input->post('lv1');
            $this->db->distinct();
            $this->db->select('lv4, keterangan');
            $data = $this->db->get_where("nomenklatur", ['level' => 4, 'lv1' => $lv1])->result_array();
            // $data = json_encode($data);
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */

     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $data = array(
            'material' => $this->put('material'),
            'kode_material' => $this->put('kode_material'),
        );

        $material = $this->put('material');
        $kode_material = $this->put('kode_material');

        $this->db->where('material', $material);
        $this->db->or_where('kode_material', $kode_material);
        $q = $this->db->get('material')->num_rows();
        if ($q > 0) {
            $this->response(
                [
                "status" => false,
                "message" => 'Nama Material atau Kode Material Sudah Terdaftar!'
            ], 200
        );
    } else {
        $this->db->where('id',$id);
        $this->db->update('material',$data);
        
        $this->response(
            [
                "status" => true,
                "message" => 'Material Diperbarui!'
                ], 200
                );
        }
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('material', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}