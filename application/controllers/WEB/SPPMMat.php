<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class SPPMMat extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id_sppm, $id_sppmmaterial = 0)
	{
        if (!empty($id_sppmmaterial)){

            $this->db->join('material a', 'a.id = b.id_material', 'left');
            $this->db->join('spesifikasi_teknik c', 'c.id = b.id_spesifikasi_teknik', 'left');
            $this->db->select('*,a.id as idmaterial, b.id as idsppmmaterial, c.id as idspesifikasiteknik');
            $data = $this->db->get_where("sppm_material b", ['b.id' => $id_sppmmaterial])->row();
        } else {
            {
                $this->db->join('material a', 'a.id = b.id_material', 'left');
                $this->db->join('spesifikasi_teknik c', 'c.id = b.id_spesifikasi_teknik', 'left');
                $this->db->select('*,a.id as idmaterial, b.id as idsppmmaterial, c.id as idspesifikasiteknik');
                $data = $this->db->get_where("sppm_material b", ['b.id_sppm' => $id_sppm])->result_array();
            }
        }

     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'no_sppm' => $this->input->post('no_sppm'),
            'id_judul' => $this->input->post('id_judul'),
            'jenis_sppm' => $this->input->post('jenis_sppm'),
            'no_spk' => $this->input->post('no_spk'),
            'no_rkm' => $this->input->post('no_rkm'),
            'forecast' => $this->input->post('forecast'),
            'stok' => $this->input->post('stok'),
            'total_pemesanan' => $this->input->post('total_pemesanan'),
            'tanggal_dibuat' => date("Y-m-d H:i:s"),
            'id_peminta' => $this->session->userdata('id'),
            'id_manager' => $this->input->post('id_manager'),
            'id_koordinator' => $this->input->post('id_koordinator'),
            'target_delivery' => $this->input->post('target_delivery'),
            'id_staf_pengadaan' => '',
            'kategori'  => 'SPPM Produk',
            'status' => 'waiting',
        );
        $this->db->insert('sppm',$data);
        
        $id_sppm = $this->db->insert_id();
        $id_judul = $this->input->post('id_judul');
        $q = $this->db->get_where('t_material',['id_judul' => $id_judul])->result_array();
        
        foreach($q as $q){
            $data2 = array(
                'id_sppm' => $id_sppm,
                'id_material' => $q['id_material']
            );

            $this->db->insert('sppm_material',$data2);
        }
        
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('sppm_material', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('sppm_material', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}