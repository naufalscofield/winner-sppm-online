<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Material extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("material", ['id' => $id])->row_array();
        }else{
            $this->db->join('nomenklatur','nomenklatur.id = material.kode_material');
            $this->db->select('*,material.id as idmaterial');
            $data = $this->db->get("material")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $data = array(
            'material' => $this->input->post('material'),
            'kode_material' => $this->input->post('kode_material'),
        );

        // $material = $this->input->post('material');
        // $kode_material = $this->input->post('kode_material');

        // $this->db->where('material', $material);
        // $this->db->or_where('kode_material', $kode_material);
        // $q = $this->db->get('material')->num_rows();
    //     if ($q > 0) {
    //         $this->response(
    //             [
    //             "status" => false,
    //             "message" => 'Nama Material atau Kode Material Sudah Terdaftar!'
    //         ], 200
    //     );
    // } else {
        $this->db->insert('material',$data);
        
        $this->response(
            [
                "status" => true,
                "message" => 'Material Ditambahkan!'
                ], 200
                );
        // }
    } 
    // public function index_post()
    // {
    //     $data = array(
    //         'material' => $this->input->post('material'),
    //         'kode_material' => $this->input->post('kode_material'),
    //     );

    //     $material = $this->input->post('material');
    //     $kode_material = $this->input->post('kode_material');

    //     $this->db->where('material', $material);
    //     $this->db->or_where('kode_material', $kode_material);
    //     $q = $this->db->get('material')->num_rows();
    //     if ($q > 0) {
    //         $this->response(
    //             [
    //             "status" => false,
    //             "message" => 'Nama Material atau Kode Material Sudah Terdaftar!'
    //         ], 200
    //     );
    // } else {
    //     $this->db->insert('material',$data);
        
    //     $this->response(
    //         [
    //             "status" => true,
    //             "message" => 'Material Ditambahkan!'
    //             ], 200
    //             );
    //     }
    // } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $data = array(
            'material' => $this->put('material'),
            'kode_material' => $this->put('kode_material'),
        );

    //     $material = $this->put('material');
    //     $kode_material = $this->put('kode_material');

    //     $this->db->where('material', $material);
    //     $this->db->or_where('kode_material', $kode_material);
    //     $q = $this->db->get('material')->num_rows();
    //     if ($q > 0) {
    //         $this->response(
    //             [
    //             "status" => false,
    //             "message" => 'Nama Material atau Kode Material Sudah Terdaftar!'
    //         ], 200
    //     );
    // } else {
        $this->db->where('id',$id);
        $this->db->update('material',$data);
        
        $this->response(
            [
                "status" => true,
                "message" => 'Material Diperbarui!'
                ], 200
                );
        // }
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('material', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}