<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Nomenklatur extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("nomenklatur", ['id' => $id])->row_array();
        }else{
            $this->db->order_by("lv1 asc");
            $data = $this->db->get("nomenklatur")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $data = $this->input->post();


        $this->db->insert('nomenklatur',$data);
        
        $this->response(
            [
                "status" => true,
                "message" => 'Nomenklatur Ditambahkan!'
                ], 200
                );
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $data = $this->put();
        $this->db->where('id',$id);
        $this->db->update('nomenklatur',$data);
        
        $this->response(
            [
                "status" => true,
                "message" => 'Material Diperbarui!'
                ], 200
                );
 
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('nomenklatur', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}