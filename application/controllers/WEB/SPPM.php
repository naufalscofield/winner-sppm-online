<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class SPPM extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
            $role = $this->session->userdata('role');
            $id_user = $this->session->userdata('id');
        
        if ($role == 'pegawai'){
            if(!empty($id)){
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users a', 'a.id = sppm.id_peminta');
                $this->db->join('users b', 'b.id = sppm.id_manager');
                $this->db->join('users c', 'c.id = sppm.id_koordinator');   
                $this->db->join('users d', 'd.id = sppm.id_staf_pengadaan', 'left');   
                $this->db->select('*,a.nm_peg as nama_peminta, b.nm_peg as nama_manager, c.nm_peg as nama_koordinator, d.nm_peg as nama_staf_pengadaan, judul.id as idjudul, sppm.id as idsppm, sppm.status as statussppm');
                $data = $this->db->get_where("sppm", ['sppm.id' => $id])->row_array();
                // print_r($data); die;
            }else
            {
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users', 'users.id = sppm.id_peminta');
                $this->db->select('*,judul.id as idjudul,users.id as iduser, sppm.id as idsppm, sppm.status as statussppm, users.status as statususer');
                $data = $this->db->get_where("sppm", ['id_peminta' => $id_user, 'kategori' => 'SPPM Produk'])->result_array();
            }
        } else if ($role == 'koordinator'){
            if(!empty($id)){
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users a', 'a.id = sppm.id_peminta');
                $this->db->join('users b', 'b.id = sppm.id_manager');
                $this->db->join('users c', 'c.id = sppm.id_koordinator');   
                $this->db->join('users d', 'd.id = sppm.id_staf_pengadaan', 'left');   
                $this->db->select('*,a.nm_peg as nama_peminta, b.nm_peg as nama_manager, c.nm_peg as nama_koordinator, d.nm_peg as nama_staf_pengadaan, judul.id as idjudul, sppm.id as idsppm, sppm.status as statussppm');
                $data = $this->db->get_where("sppm", ['sppm.id' => $id])->row_array();
            }else
            {
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users', 'users.id = sppm.id_peminta');
                $this->db->select('*,judul.id as idjudul,users.id as iduser, sppm.id as idsppm, sppm.status as statussppm, users.status as statususer');
                $data = $this->db->get_where("sppm", ['id_koordinator' => $id_user, 'sppm.status' => 'acc_peminta', 'kategori' => 'SPPM Produk'])->result_array();
            }
        } else if ($role == 'manager'){
            if(!empty($id)){
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users a', 'a.id = sppm.id_peminta');
                $this->db->join('users b', 'b.id = sppm.id_manager');
                $this->db->join('users c', 'c.id = sppm.id_koordinator');   
                $this->db->join('users d', 'd.id = sppm.id_staf_pengadaan', 'left');   
                $this->db->select('*,a.nm_peg as nama_peminta, b.nm_peg as nama_manager, c.nm_peg as nama_koordinator, d.nm_peg as nama_staf_pengadaan, judul.id as idjudul, sppm.id as idsppm, sppm.status as statussppm');
                $data = $this->db->get_where("sppm", ['sppm.id' => $id])->row_array();
            }else
            {
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users', 'users.id = sppm.id_peminta');
                $this->db->select('*,judul.id as idjudul,users.id as iduser, sppm.id as idsppm, sppm.status as statussppm, users.status as statususer');
                $data = $this->db->get_where("sppm", ['id_manager' => $id_user, 'sppm.status' => 'acc_koordinator', 'kategori' => 'SPPM Produk'])->result_array();
            }
            
        } else {
            if(!empty($id)){
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users a', 'a.id = sppm.id_peminta');
                $this->db->join('users b', 'b.id = sppm.id_manager');
                $this->db->join('users c', 'c.id = sppm.id_koordinator');   
                $this->db->join('users d', 'd.id = sppm.id_staf_pengadaan', 'left');   
                $this->db->select('*,a.nm_peg as nama_peminta, b.nm_peg as nama_manager, c.nm_peg as nama_koordinator, d.nm_peg as nama_staf_pengadaan, judul.id as idjudul, sppm.id as idsppm, sppm.status as statussppm');
                $data = $this->db->get_where("sppm", ['sppm.id' => $id])->row_array();
            }else
            {
                $this->db->join('judul', 'judul.id = sppm.id_judul');
                $this->db->join('users', 'users.id = sppm.id_peminta');
                $this->db->select('*,judul.id as idjudul,users.id as iduser, sppm.id as idsppm, sppm.status as statussppm, users.status as statususer');
                $acc = ['acc_manager', 'acc_pengadaan'];
                $this->db->where_in('sppm.status',$acc);
                $this->db->where('kategori','SPPM Produk');
                $data = $this->db->get("sppm")->result_array();
            }

        }


     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
       
        
        date_default_timezone_set('Asia/Jakarta');
        $data = array(
            // 'no_sppm' => $this->input->post('no_sppm'),
            'id_judul' => $this->input->post('id_judul'),
            'jenis_sppm' => $this->input->post('jenis_sppm'),
            'no_spk' => $this->input->post('no_spk'),
            'no_rkm' => $this->input->post('no_rkm'),
            // 'forecast' => $this->input->post('forecast'),
            'stok' => $this->input->post('stok'),
            'total_pemesanan' => $this->input->post('total_pemesanan'),
            'tanggal_dibuat' => date("Y-m-d H:i:s"),
            'id_peminta' => $this->session->userdata('id'),
            'id_manager' => $this->input->post('id_manager'),
            'id_koordinator' => $this->input->post('id_koordinator'),
            'target_delivery' => $this->input->post('target_delivery'),
            // 'id_staf_pengadaan' => '',
            'kategori'  => 'SPPM Produk',
            'status' => 'waiting',
        );
        $this->db->insert('sppm',$data);
        
        $id_sppm = $this->db->insert_id();
        $id_judul = $this->input->post('id_judul');
        $q = $this->db->get_where('t_material',['id_judul' => $id_judul])->result_array();
        
        foreach($q as $q){
            $data2 = array(
                'id_sppm' => $id_sppm,
                'id_material' => $q['id_material']
            );

            $this->db->insert('sppm_material',$data2);
        }
        
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('sppm', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('users', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}