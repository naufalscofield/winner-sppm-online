<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Pegawai extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("users", ['id' => $id])->row_array();
        }else{
            $this->db->join('biro', 'biro.id = users.id_biro');
            $this->db->select('*,biro.id as idbiro,users.id as iduser');
            $data = $this->db->get("users")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        // $input = $this->input->post();
        $data = array(
            'nip' => $this->input->post('nip'),
            'nm_peg' => $this->input->post('nm_peg'),
            'password' => md5('winner@123'),
            'status' => $this->input->post('status'),
            'jns_kelamin_peg' => $this->input->post('jns_kelamin_peg'),
            'alamat' => $this->input->post('alamat'),
            'kodepos' => $this->input->post('kodepos'),
            'telepon' => $this->input->post('telepon'),
            'handphone' => $this->input->post('handphone'),
            'email' => $this->input->post('email'),
            'kd_jabatan' => $this->input->post('kd_jabatan'),
            'nm_jabatan' => $this->input->post('nm_jabatan'),
            'kd_unit_org' => $this->input->post('kd_unit_org'),
            'nm_unit_org' => $this->input->post('nm_unit_org'),
            'id_biro' => $this->input->post('id_biro'),
            'admin' => $this->input->post('admin'),
            'superadmin' => $this->input->post('superadmin'),
            'role' => $this->input->post('role'),
            'fcm_token' => null
        );
        $this->db->insert('users',$data);
     
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($nip)
    {
        $input = $this->put();
        $this->db->update('users', $input, array('nip'=>$nip));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('users', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}