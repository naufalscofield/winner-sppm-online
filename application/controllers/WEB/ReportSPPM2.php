<?php
require 'vendor/autoload.php';
    
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

    class ReportSPPM2 extends CI_Controller{
        
        public function __construct()
        {
            parent::__construct();
        }
        
        public function index($id_sppm)
        {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'Hello World !');

            $writer = new Xlsx($spreadsheet);
            $writer->save('hello world.xlsx');
        }
    }
?>