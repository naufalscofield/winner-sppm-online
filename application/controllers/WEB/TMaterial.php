<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class TMaterial extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id_judul = 0)
	{
        if(!empty($id_judul)){
            $this->db->join('judul a','a.id = c.id_judul');
            $this->db->join('material b','b.id = c.id_material');
            $this->db->select('*,a.id as idjudul, b.id as idmaterial, c.id as idtmaterial');
            $data = $this->db->get_where("t_material c", ['c.id_judul' => $id_judul])->result_array();
        }else{
            $data = $this->db->get("t_material")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $id_judul = $this->input->post('id_judul');
        $material = $this->input->post('material');

        foreach ($material as $m) {
            $data = array(
                'id_judul' => $id_judul,
                'id_material' => $m
            );
        $this->db->insert('t_material',$data);
        }
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('judul', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        // $this->db->delete('judul', array('id'=>$id));
        $this->db->delete('t_material', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }
    	
}