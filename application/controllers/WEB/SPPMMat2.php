<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class SPPMMat2 extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_post()
    {
        $id_sppm =  $this->input->post('id_sppm');
            $data = array(
                'id_sppm' => $id_sppm,
                'material_mat' => $this->input->post('material_mat'),
                'merek' => $this->input->post('merek'),
                'harga' => $this->input->post('harga'),
                'qty' => $this->input->post('qty'),
                'satuan' => $this->input->post('satuan'),
                'spesifikasi_teknik_mat' => $this->input->post('spesifikasi_teknik_mat'),
            );

        $this->db->insert('sppm_material',$data);
        $this->response(['Item created successfully.'], REST_Controller::HTTP_OK);
    } 

    public function index_get($id_sppm, $id_sppmmaterial = 0)
	{
        if (!empty($id_sppmmaterial)){
            $data = $this->db->get_where("sppm_material b", ['b.id' => $id_sppmmaterial])->row();
        } else {
            {
                $data = $this->db->get_where("sppm_material b", ['b.id_sppm' => $id_sppm])->result_array();
            }
        }

        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    public function index_delete($id)
    {
        return $this->db->delete('sppm_material',['id' => $id]);
        
        $this->response($data, REST_Controller::HTTP_OK);
    }
    	
}